-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2019 at 04:25 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `kode_buku` int(11) NOT NULL,
  `id` varchar(25) NOT NULL,
  `kode_rak` varchar(5) NOT NULL,
  `kode_kategori` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `pengarang` varchar(100) NOT NULL,
  `penerbit` varchar(100) NOT NULL,
  `thn_terbit` varchar(4) NOT NULL,
  `asal` varchar(20) NOT NULL,
  `jumlah` int(12) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `foto` varchar(25) NOT NULL,
  `deskripsi` varchar(25) NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`kode_buku`, `id`, `kode_rak`, `kode_kategori`, `judul`, `pengarang`, `penerbit`, `thn_terbit`, `asal`, `jumlah`, `kondisi`, `foto`, `deskripsi`, `status`) VALUES
(1, 'REF001', 'REF', 'REF', 'Encyclopedia Of Knowledge ', 'Grolier', 'Grolier Incorporated', '1994', 'HADIAH', 20, 'Baik', '-', '-', '0'),
(2, 'REF002', 'REF', 'REF', 'New Websters Dictionary', '-', '-', '1992', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(3, 'REF003', 'REF', 'REF', 'The New Grolier Webster International Dictionary Of The English Language', '-', '-', '1975', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(4, 'REF004', 'REF', 'REF', 'Bussiness and society', 'Donna J. Widodo', 'Harper Collins Publisher', '1990', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(5, 'REF005', 'REF', 'REF', 'Principles of Economics 4th Edition', 'Roy J. Ruffin', '-', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(6, 'REF006', 'REF', 'REF', 'International Travel and Tourism', 'Donald E. Lundberg', 'John Willey and Sons', '1985', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(7, 'REF007', 'REF', 'REF', 'The Economics Of Money, Banking and Financial Market', 'Rfrederic S. Mishkin', 'Harper Collins Publisher', '1992', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(8, 'REF008', 'REF', 'REF', 'Sales Management', 'Eugene M. Jonhson', 'Mc Graw-Hill Book Company', '1992', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(9, 'REF009', 'REF', 'REF', 'Business Communications 3rd Edition', 'Raymond A. Dummon', 'Harper Collins Publisher', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(10, 'REF010', 'REF', 'REF', 'Management and Performance', 'Andrew D. Szilagyi', 'Scott, Foresman and  Company', '1988', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(11, 'REF011', 'REF', 'REF', 'Principle Of Insurance 3rd Edition', 'George E. Rejda', 'Scott, Foresman and  Company', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(12, 'REF012', 'REF', 'REF', 'Principle Of Marketing 3rd Edition', 'Thomas C. Kinnear', 'Scott, Foresman and  Company', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(13, 'REF013', 'REF', 'REF', 'Business Comunication Writing', 'Randall E. Mayors', 'Happer and Row Publisher', '1990', 'HADIAH', 5, 'Baik', '-', '-', '0'),
(14, 'REF014', 'REF', 'REF', 'Basic Economic', 'Paul R. Gregory', 'Harper Collins Publisher', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(15, 'REF015', 'REF', 'REF', 'Principle Of Marketing 5th Edition', 'Philip Kotler ', 'Prentice Hall', '1991', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(16, 'REF016', 'REF', 'REF', 'Traveling Creative Workshop', 'Hanley Norins', 'Prentice Hall', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(17, 'REF017', 'REF', 'REF', 'Statistic For Business and Economics', 'Robert Sandy', 'Mc.Graw-Hilllinc', '1990', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(18, 'REF018', 'REF', 'REF', 'Communication Management and Business 5th Edition', 'Norman B. Sigband', 'Scott, Foresman and  Company', '1969', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(19, 'REF019', 'REF', 'REF', 'Developing Management Skills 2nd Edition', 'David A. Whetten', 'Harper Collins Publisher', '1991', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(20, 'REF020', 'REF', 'REF', 'Understanding Marketing', 'Alan West', 'Happer and Row Publisher', '1987', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(21, 'REF021', 'REF', 'REF', 'Learning From Case Studies', 'Geof Easton', 'Prentice Hall', '1982', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(22, 'REF022', 'REF', 'REF', 'Pikiran', 'John Rowan Wilson', 'Tira Pustaka Jakarta', '1964', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(23, 'REF023', 'REF', 'REF', 'Waktu (edisi kedua)', 'Samuel A. Goudsmit', 'Tira Pustaka Jakarta', '1986', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(24, 'REF024', 'REF', 'REF', 'The Americana', 'Bernard S Cayne', 'Grolier Incorporated', '1976', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(25, 'REF025', 'REF', 'REF', 'Ilmu Pengetahuan Populer (Jilid 1- Jilid 10)', 'Bernard S Cayne', 'Grolier Incorporated', '1976', 'HADIAH', 10, 'Baik', '-', '-', '0'),
(26, 'REF026', 'REF', 'REF', 'Essentials Of Economics (Second Edition)', 'Paulr Gregory', 'Harper Collins Publisher', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(27, 'REF027', 'REF', 'REF', 'Small Management Fundamentals', 'Dan Steinoff John', 'Mc. Graw-Hill', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(28, 'REF028', 'REF', 'REF', 'Kamus Lengkap Ekonomi', 'Drs. Ahmad Antoni', 'Gita Media Press', '2003', 'HADIAH', 5, 'Baik', '-', '-', '0'),
(29, 'REF029', 'REF', 'REF', 'Kamus Istilah Ekonomi (Ensiklopedi Mini)  Inggris-Belanda-Indonesia', 'Dr. Winardi, SE', 'PT. Bina Aksara Jakarta', '1991', 'HADIAH', 15, 'Baik', '-', '-', '0'),
(30, 'REF030', 'REF', 'REF', 'Principles Of Money, Banking and Financial Market (seven edition)', 'Lawrence S. Ritter', 'Basic Book', '1974', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(31, 'REF031', 'REF', 'REF', 'Money and Banking', 'Dudley G Luckett', 'Mc. Graw-Hill', '1984', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(32, 'REF032', 'REF', 'REF', 'Bank Strategic Management And Marketing', 'Derek F Citannon', 'John Wiley and Sons', '1986', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(33, 'REF033', 'REF', 'REF', 'Kamus Hukum (Edisi Baru)', 'Drs Sudarsono, SH. Msi', 'Rineka Cipta', '2007', 'HADIAH', 5, 'Baik', '-', '-', '0'),
(34, 'REF034', 'REF', 'REF', 'Public AdministrationUnderstanding Management, Politic, And Law In the Public', 'David H. Rosenblcom', 'Mc. Graw Hill Inc', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(35, 'REF035', 'REF', 'REF', 'Seling Principles and Practices Thirteent Edition', 'Richard H.Buskirk', 'Mc. Graw Hill Inc', '1992', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(36, 'REF036', 'REF', 'REF', 'Negara dan bangsa', 'Grolier', 'Grolier International Inc', '1989', 'HADIAH', 10, 'Baik', '-', '-', '0'),
(37, 'REF037', 'REF', 'REF', 'Kamus Istilah Komputer', 'Drs. Bob Widyahartono', 'Alumni/1085/Bandung Kotak Pos 272', '1982', 'HADIAH', 5, 'Baik', '-', '-', '0'),
(38, 'REF038', 'REF', 'REF', 'Kamus Besar Bahasa Indonesia', 'Tim Penyusun Kamus Pusat Pembinaan dan Pengembangan bahasa', 'Balai Pustaka', '1990', 'HADIAH', 13, 'Baik', '-', '-', '0'),
(39, 'REF039', 'REF', 'REF', 'Kamus Umum Bahasa indonesia', 'W.J.S. Poerwadarminta', 'Balai Pustaka', '1991', 'HADIAH', 13, 'Baik', '-', '-', '0'),
(40, 'REF040', 'REF', 'REF', 'Kamus Lengkap Bahasa Indonesia', 'Kamisa', 'Kartika', '1997', 'HADIAH', 5, 'Baik', '-', '-', '0'),
(41, 'REF041', 'REF', 'REF', 'Kamus Lengkap Bahasa Jawa', 'S.A. Manguhsuwito', 'CV Yrma Widya', '2002', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(42, 'REF042', 'REF', 'REF', 'Complete Secretarys Handbook Sixth Edition', 'Lilian Doris', 'Prentice Hall', '1990', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(43, 'REF043', 'REF', 'REF', 'File Structures Theory And Practice', 'Panos E. Livadas', 'Prentice Hall', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(44, 'REF044', 'REF', 'REF', 'Business English And Communication', 'Clark. Zimmer. ', 'Glencoe Macmillan', '1988', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(45, 'REF045', 'REF', 'REF', 'Kamus Perbankan Inggris-Indonesia', 'O.P.Simorangkir', 'Bina Aksara', '1989', 'HADIAH', 19, 'Baik', '-', '-', '0'),
(46, 'REF046', 'REF', 'REF', 'Kamus Umum Lengkap Inggris-Indonesia', 'Saodah Nasution', 'Mutiara Sumber  Widya', '1990', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(47, 'REF047', 'REF', 'REF', 'Ensiklopedia Matematika', 'ST. Negoro B. Harahap', 'Ghalia Indonesia', '2005', 'HADIAH', 4, 'Baik', '-', '-', '0'),
(48, 'REF048', 'REF', 'REF', 'Kamus Lengkap Inggris-Indonesia', 'Farhan Fadhli', 'Halim Jaya', '1998', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(49, 'REF049', 'REF', 'REF', 'Kamus Lengkap Indonesia-Jepang', 'Team Kashiko', 'Kashiko', '2004', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(50, 'REF050', 'REF', 'REF', 'Energi', 'Mitchell Wilson', 'Tira Pustaka', '1987', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(51, 'REF051', 'REF', 'REF', 'Molekul Raksasa', 'Herman F. Mark', 'Tira Pustaka', '1980', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(52, 'REF052', 'REF', 'REF', 'Air (Edisi Kedua)', 'Luna B. Leopold Kenneth S. Davis', 'Tira Pustaka', '1987', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(53, 'REF053', 'REF', 'REF', 'Perabot Pelituran Sendiri', 'John Savage', 'Plenary Publicatitions International', '1984', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(54, 'REF054', 'REF', 'REF', 'Kantong Serbaguna', 'John Bracciani', 'Plenary Publications International', '1985', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(55, 'REF055', 'REF', 'REF', 'Patung', 'Peter Lipman', 'Plenary Publications International', '1982', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(56, 'REF056', 'REF', 'REF', 'Menandai Perak', 'William Knight', 'Plenary Publications International', '1985', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(57, 'REF057', 'REF', 'REF', 'Merambah Pantai', 'J Frederick North', 'Plenary Publications International', '1982', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(58, 'REF058', 'REF', 'REF', 'Motif Rajutan Tradisional', 'Helen Maris', 'Plenary Publications International', '1984', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(59, 'REF059', 'REF', 'REF', 'Selai Dan Buah Awetan', 'Eva Pardey', 'Plenary Publications International', '1985', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(60, 'REF060', 'REF', 'REF', 'Kotak', 'John Noblitt', 'Plenary Publications International', '1985', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(61, 'REF061', 'REF', 'REF', 'Sulam Bingkai ', 'Bernice Barlays', 'Pienary Publications International', '1984', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(62, 'REF062', 'REF', 'REF', 'Menciptakan Musik', 'Robert Wood', 'Plenary Publications International', '1982', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(63, 'REF063', 'REF', 'REF', 'Cuaca (Edisi Ke-2)', 'Philip D. Thompson', 'Tira Pustaka', '1987', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(64, 'REF064', 'REF', 'REF', 'Perilaku Binatang (Edisi Ke-2)', 'Niko Tinbergen', 'Tira Pustaka', '1987', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(65, 'REF065', 'REF', 'REF', 'Ilmuwan', 'Henry Margenau', 'Tira Pustaka', '1987', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(66, 'REF066', 'REF', 'REF', 'Medical & Health Encyclopedia', 'Morris Freshbein', 'H.S. Stuttman Co.Inc', '1974', 'HADIAH', 4, 'Baik', '-', '-', '0'),
(67, 'REF067', 'REF', 'REF', 'Makanan Dan Gizi (Edisi Ke-2)', 'William H. Sebrell', 'Tira Pustaka', '1986', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(68, 'REF068', 'REF', 'REF', 'Insinyur', 'C.C. Furnas Joe', 'Tira Pustaka', '1986', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(69, 'REF069', 'REF', 'REF', 'Mesin', 'Robert O\'Brein', 'Tira Pustaka', '1986', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(70, 'REF070', 'REF', 'REF', 'Business Studies', 'David Needham', 'Mc Graw-Hill International', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(71, 'REF071', 'REF', 'REF', 'Kamus Istilah Manajemen', 'Panitia Istilah Manajemen', 'Balai Askara', '1983', 'HADIAH', 16, 'Baik', '-', '-', '0'),
(72, 'REF072', 'REF', 'REF', 'Kamus Istilah Akuntansi', 'R.A. Fadly', 'Ghalia Indonesia', '1990', 'HADIAH', 13, 'Baik', '-', '-', '0'),
(73, 'REF073', 'REF', 'REF', 'Dictionary of Accainting Revised Edition Kamus Akutansi', 'Assegaf ibrahim Abdullah', 'PT Mario Grafika', '1991', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(74, 'REF074', 'REF', 'REF', 'Business Management', 'Robert erskine', 'Prentice Hall', '1991', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(75, 'REF075', 'REF', 'REF', 'Bank Management', 'George H. Hempel', 'John wiley', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(76, 'REF076', 'REF', 'REF', 'Modern Business', 'Alexander Homilton Instiute', 'Grolier Inc', '1970', 'HADIAH', 20, 'Baik', '-', '-', '0'),
(77, 'REF077', 'REF', 'REF', 'Small Business Success', 'Paul Folaj Haword Green', 'Paul Chopman Publshin Ltd', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(78, 'REF078', 'REF', 'REF', 'Financial Management 2 Edition', 'Anthony G. Puxty j. Collin Dodds', 'Elbs & Chapenan&Hall', '1991', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(79, 'REF079', 'REF', 'REF', 'The Small Business Financial Planner', 'Gregory R. Glau', 'John Wiley ', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(80, 'REF080', 'REF', 'REF', 'Principles of Management Information System', 'George M. Scott', 'Mc. Grow-Hill', '1986', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(81, 'REF081', 'REF', 'REF', 'Prensentations for Desicron Makers', 'Marya W. Hokombe Judith K. Stein', 'Van Nostrand Reinhold', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(82, 'REF082', 'REF', 'REF', ' Business Communication (3rd)', 'Courtland Lbovce John V. Thill', 'Mc. Grow-Hill Inc', '1992', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(83, 'REF083', 'REF', 'REF', 'Business Communication Skills & Strategis', 'Jone W. Gibson Richard M. Hodgetts', 'Harper&Row', '1990', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(84, 'REF084', 'REF', 'REF', 'Marketing Management', 'Philip Kotler', 'Prentice Hall,Inc', '1991', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(85, 'REF085', 'REF', 'REF', 'How To Market To Consumers', 'John A. Quclch', 'John Wiley &Sons', '1989', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(86, 'REF086', 'REF', 'REF', 'Marketing Manangement and Administrative', 'Stell art Henderson dkk', 'Mo.Grow-Hill', '1983', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(87, 'REF087', 'REF', 'REF', 'Marketing Research 4 Edition', 'David A.Aoker Geor george S. Day', 'John Wiley& Sons Inc', '1990', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(88, 'REF088', 'REF', 'REF', 'Marketing Today A Retail Focus', 'Harold J.Stoyles', 'Mc. Grow-Hill Ryer son', '1988', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(89, 'REF089', 'REF', 'REF', 'Human Retation In Organization', 'V.8.Jenks', 'Harper&Row Publis hers', '1990', 'HADIAH', 3, 'Baik', '-', '-', '0'),
(90, 'REF090', 'REF', 'REF', 'Roda', 'Wilfred Owen Ezra  Bowen', 'Tira pustika', '1985', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(91, 'REF091', 'REF', 'REF', 'Materi', 'Raplh.E. Lapp', 'Tira Pustaka', '1983', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(92, 'REF092', 'REF', 'REF', 'Penuntun Perawatan & Pengobatan Modern', 'Harold Shryock MD', 'Indonesia Publishing House', '1994', 'HADIAH', 2, 'Baik', '-', '-', '0'),
(93, 'REF093', 'REF', 'REF', 'Kesehatan & Penyakit', 'Rere Dubos', 'Tira Pustaka', '1985', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(94, 'REF094', 'REF', 'REF', 'Kamus Manajemen', 'Moekijat', 'CV Mandar Maju', '1990', 'HADIAH', 16, 'Baik', '-', '-', '0'),
(95, 'REF095', 'REF', 'REF', 'Do It Your Self', '-', 'H.S. Stuttman Co.Inc', '1973', 'HADIAH', 21, 'Baik', '-', '-', '0'),
(96, 'REF096', 'REF', 'REF', 'Ensiklopedia Geografi', 'Marbun', 'Yudhistira', '2004', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(97, 'REF097', 'REF', 'REF', 'The Family Home Cook Book', 'Melanie De Proft', 'Lexian Pubslication', '1973', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(98, 'REF098', 'REF', 'REF', 'Encyclopedia Americana', 'A To Anpu', 'Americana Corp.', '1829', 'HADIAH', 30, 'Baik', '-', '-', '0'),
(99, 'REF099', 'REF', 'REF', 'Louis Pasteur', 'Beverley Brech', 'Gramedia Pustaka Utama', '1993', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(100, 'REF100', 'REF', 'REF', 'Bunda Teresa', 'Charlotte Gray', 'Gramedia Pustaka Utama', '1994', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(101, 'REF101', 'REF', 'REF', 'James Watt', 'Anna Sproule', 'Gramedia Pustaka Utama', '1992', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(102, 'REF102', 'REF', 'REF', 'Thomas A. Edison', 'Anna Sproule', 'Gramedia Pustaka Utama', '1992', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(103, 'REF103', 'REF', 'REF', 'Johann Gutenberg', 'Michael Pollard', 'Gramedia Pustaka Utama', '1993', 'HADIAH', 1, 'Baik', '-', '-', '0'),
(104, 'REF104', 'REF', 'REF', 'Wright Bersaudara', 'Anna Sproule', 'Gramedia Pustaka', '1992', 'HADIAH', 1, 'Baik', '-', '-', '0');

-- --------------------------------------------------------

--
-- Table structure for table `detail_gpeminjaman`
--

CREATE TABLE `detail_gpeminjaman` (
  `id_gpinjam` varchar(25) NOT NULL,
  `kode_buku` varchar(25) NOT NULL,
  `jml` int(11) NOT NULL,
  `jml2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_peminjaman`
--

CREATE TABLE `detail_peminjaman` (
  `id_pinjam` int(11) NOT NULL,
  `kode_buku` varchar(25) NOT NULL,
  `jml` int(11) NOT NULL,
  `jml2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_peminjaman`
--

INSERT INTO `detail_peminjaman` (`id_pinjam`, `kode_buku`, `jml`, `jml2`) VALUES
(1, '2', 1, 0),
(1, '10', 1, 0),
(2, '4', 1, 0),
(2, '8', 1, 0),
(3, '7', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(250) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gpeminjam`
--

CREATE TABLE `gpeminjam` (
  `id_gpinjam` int(11) NOT NULL,
  `no_anggota` varchar(25) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gpengunjung`
--

CREATE TABLE `gpengunjung` (
  `id_gpengunjung` int(11) NOT NULL,
  `no_anggota` varchar(25) NOT NULL,
  `tanggal` datetime NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `no_anggota` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`no_anggota`, `nama`, `jabatan`, `alamat`) VALUES
('195603231989032003', 'Dra SITI WAHYUNI', 'Guru', 'Jl Kamboja III/30 Tgl'),
('195705081990031003', 'M DJEN FAHRUDIN, BSc', 'Guru', 'Jl HOC Cokroaminoto 36 Tgl'),
('195810011989031008', 'Drs KODIQ', 'Guru', 'Jl Argopuro, Rambigundam'),
('195812311986031131', 'Drs MULYONO, MPd', 'Guru', 'Jl PB. Sudirman 111 Tgl'),
('195907211991032005', 'DENOK ISDIANA, SPd', 'Guru', 'Jl. Mangga no. 120 Desa Tgl Wetan'),
('196109271992112003', 'Hj SUHARTATIK, SPd', 'Guru', 'Jl Sawo no 88 Tgl Wetan'),
('196201041983031010', 'Drs H WAHONO', 'Guru', 'Jl Argopuro 57 Manggisan '),
('196203171990031008', 'Drs SUTAMAN', 'Guru', 'Desa Tegalwangi Paleran '),
('196203262008012004', 'Dra ENDARWATI', 'Guru', 'Perum Pondok Tanggul Asri P8'),
('196206182008011002', 'Drs M. NATSIR', 'Guru', 'Jl Jagalan I/ 38A Ambulu '),
('196206201987031008', 'Drs YUNI IRIANTO', 'Guru', 'Perum Wisma Handayani Tgl Wetan'),
('196304022007011012', 'Drs BIBIT ', 'Guru', 'Dusun Kemukuh Tembokrejo'),
('196304111999031003', 'Drs HM HARI SUBAGYO, MMPd', 'Guru', 'Perumnas POM 49 Tanggul'),
('196311041987031008', 'Drs SUPANDI', 'Guru', 'Jl Durian 36 Tanggul Wetan'),
('196401152007012006', 'Dra SITI AISAH', 'Guru', 'Jl Rambutan Gambirono'),
('196403172007012005', 'Dra ERNANIK ANDARWATI', 'Guru', 'Jl Nias IV/1 Jbr'),
('196407031990031015', 'Drs DWI CAHYONO', 'Guru', 'Jl PB Sudirman Tanggul'),
('196407171989032014', 'Dra. PRIWAHYU HARTANTI, M.Pd.', 'Kepsek', 'Jember'),
('196408021989031014', 'AGUS WARDOYO, SE', 'Guru', 'Perumnas POM 50 Tgl'),
('196408071998022001', 'Dra WIDARSIH', 'Guru', 'Perumnas POM Tgl'),
('196411171989032004', 'Hj ISNAYAH, SPd, MMPd', 'Guru', 'Perumnas POM 49 Tgl'),
('196412232007011006', 'Drs OESDIARKO TJAHJO K', 'Guru', 'Jl Bangka IV/25 Jember 18121'),
('196504081990031021', 'PAMUJIYANTO, SE', 'Guru', 'Jl PB Sudirman (Blkg Puskesmas) Tgl'),
('196506101994032009', 'Dra TITIK SUDARWATI', 'Guru', 'Perm Pondok Tgl Asri Blok I/13 Tgl'),
('196506182000122001', 'Ir Hj USWATUN K', 'Guru', 'Jl Blimbing 26 Sidomekar'),
('196509101990031015', 'Drs SUPARNO', 'Guru', 'Jl PB Sudirman 126 Tanggul'),
('196601252008012005', 'LILIS SURYANI, SPd', 'Guru', 'Jl Nangka 78 Tgl Wetan'),
('196612191994122002', 'Dra LULUK HANUM', 'Guru', 'Jl Pelita 10 Sidomekar Semboro'),
('196704111990012002', 'ERNI SUMARLIAH, SPd', 'Guru', 'Semboro Lor Kec Semboro'),
('196805031998021003', 'Drs HASYIM', 'Guru', 'Jl Darmawangsa Kaliwining '),
('196808212007011016', 'AGUS ISNURYANTO, SPd', 'Guru', 'Prm Tg Besar Permai II Blok T No 02'),
('196811292007012018', 'Dra TRI RAHAYUNINGSIH', 'Guru', 'Jl Dr. Sutomo I/1 Rambipuji '),
('196908182006042006', 'Dra RINA SILVIA DEWAJANI', 'Guru', 'Perum Mastrip Blok M4 Jember'),
('196908232007012014', 'RR RETNO SRI UTAMI, SPd', 'Guru', 'Griya Mangli Indah DK 20 Jember'),
('197101171997031003', 'YAKOP JUNAIDI, SPd', 'Guru', 'Jl Diponegoro 71 Tanggul'),
('197103302007012008', 'SURITA, SPd', 'Guru', 'Jl Rambutan 59 Sebanen Jombang'),
('197205012007012009', 'ANITA VOLIYANTI, SPd', 'Guru', 'Prm Bumi Wirolegi Permai CB-4 Jbr'),
('197311261999031001', 'ABD HAYYI, SPd', 'Guru', 'Jl Argopuro I/94 Manggisan'),
('197406062008012024', 'AINUN RIFA MASRUROH, SPd', 'Guru', 'Paleran Umbulsari'),
('197807082010012009', 'VERRA JULIANI, SSos, MM', 'Guru', 'Jl PB. Sudirman I/15 Jbr'),
('198505222010011012', 'DIDIK IRYANTO, SPd', 'Guru', 'Sukorejo Bangorejo Banyuwangi');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` varchar(25) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
('AKL', 'AKUNTANSI KEUANGAN LEMBAGA'),
('BDP', 'BISNIB DARING DAN PEMASARAN'),
('MM', 'MULTIMEDIA'),
('OTKP', 'OTOMATISASI DAN TATA KELOLA PERKANTORAN'),
('RPL', 'REKAYASA PERANGKAT LUNAK');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `kategori`) VALUES
(1, 'KARYA UMUM'),
(2, 'FILSAFAT'),
(3, 'AGAMA'),
(4, 'ILMU SOSIAL'),
(5, 'BAHASA'),
(6, 'ILMU-ILMU MURNI'),
(7, 'ILMU TERAPAN'),
(8, 'SENI & OLAHRAGA'),
(9, 'SASTRA'),
(10, 'SEJARAH');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kejur` varchar(25) NOT NULL,
  `id_jurusan` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kejur`, `id_jurusan`) VALUES
('10 MM 1', 'MM'),
('10 MM 2', 'MM'),
('10 MM 3', 'MM'),
('11 MM 1', 'MM'),
('11 MM 2', 'MM'),
('11 MM 3', 'MM'),
('12 MM 1', 'MM'),
('12 MM 2', 'MM'),
('10 OTKP 1', 'OTKP'),
('10 OTKP 2', 'OTKP'),
('10 OTKP 3', 'OTKP'),
('11 OTKP 1', 'OTKP'),
('11 OTKP 2', 'OTKP'),
('12 OTKP 1', 'OTKP'),
('12 OTKP 2', 'OTKP'),
('10 RPL 1', 'RPL'),
('10 RPL 2', 'RPL'),
('11 RPL 1', 'RPL'),
('11 RPL 2', 'RPL'),
('12 RPL 1', 'RPL'),
('12 RPL 2', 'RPL');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `komentar` varchar(2000) NOT NULL,
  `rating` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nip` int(25) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `jabatan` int(1) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `user`, `password`, `nip`, `nama_pegawai`, `jenis_kelamin`, `jabatan`, `alamat`, `telp`, `status`) VALUES
(1, 'asrsaif04', '52c305280518a633a85558a4985811fa', 1234567876, 'saifur', 'L', 1, 'tanggul', '0000876', 1),
(2, 'saifur04', '06c1167e1b4f393624111dc3aebf65b6', 1234567876, 'saifur', 'L', 2, 'tanggul', '0000876', 1);

-- --------------------------------------------------------

--
-- Table structure for table `peminjam`
--

CREATE TABLE `peminjam` (
  `id_pinjam` int(11) NOT NULL,
  `nis` varchar(25) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjam`
--

INSERT INTO `peminjam` (`id_pinjam`, `nis`, `tgl_pinjam`, `tgl_kembali`, `keterangan`, `status`) VALUES
(1, '12784817065', '2019-03-06', '2019-03-04', '', 1),
(2, '12791824065', '2019-03-06', '2019-03-07', '', 1),
(3, '12795828065', '2019-03-06', '2019-03-06', 'tes', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pengunjung`
--

CREATE TABLE `pengunjung` (
  `id_pengunjung` int(11) NOT NULL,
  `nis` varchar(25) NOT NULL,
  `tanggal` datetime NOT NULL,
  `keterangan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengunjung`
--

INSERT INTO `pengunjung` (`id_pengunjung`, `nis`, `tanggal`, `keterangan`) VALUES
(2, '12784817065', '2019-03-04 14:37:47', 'Meminjam buku'),
(3, '12782815065', '2019-03-04 14:37:55', 'Mengembalikan buku'),
(4, '12808841065', '2019-03-05 12:53:17', 'Mengembalikan buku'),
(5, '12879242067', '2019-03-05 14:07:24', 'Meminjam buku'),
(6, '12882245067', '2019-03-05 14:07:34', 'Mengembalikan buku');

-- --------------------------------------------------------

--
-- Table structure for table `perpus`
--

CREATE TABLE `perpus` (
  `id_perpus` varchar(25) NOT NULL,
  `lokasi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perpus`
--

INSERT INTO `perpus` (`id_perpus`, `lokasi`) VALUES
('p1', 'perpustakaan 1'),
('p2', 'perpustakaan 2');

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `kode_rak` varchar(5) NOT NULL,
  `id_perpus` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`kode_rak`, `id_perpus`) VALUES
('000', 'p1'),
('100', 'p2'),
('200', 'p2'),
('300', 'p2'),
('400', 'p2'),
('500', 'p2'),
('600', 'p2'),
('700', 'p2'),
('800', 'p2'),
('900', 'p2');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(25) NOT NULL,
  `tgl_terima` varchar(25) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `id_kejur` varchar(25) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `tgl_terima`, `nama_siswa`, `id_kejur`, `alamat`, `jenis_kelamin`, `tempat_lahir`, `tgl_lahir`, `email`) VALUES
('12778811065', '17/07/2017', 'ABDILLAH ARIFIN', 'XI RPL 1', 'Teko\'an', '', '-', '', '-'),
('12779812065', '17/07/2017', 'ACHMAD ROFIQ HAMDANI', 'XI RPL 1', 'Sumberbaru', '-', '-', '-', '-'),
('12780813065', '17/07/2017', 'ADAM KUSUMA WARDANA', 'XI RPL 1', 'Rowo Tengah', '-', '-', '-', '-'),
('12781814065', '17/07/2017', 'ADILLA FATMA PUTRI', 'XI RPL 1', 'Sumber Agung', '-', '-', '-', '-'),
('12782815065', '17/07/2017', 'AHMAD FAISAL', 'XI RPL 1', 'Gambirono', '-', '-', '-', '-'),
('12783816065', '17/07/2017', 'AHMAD RIZKI KUSNAIN', 'XI RPL 1', 'Bangsalsari', '-', '-', '-', '-'),
('12784817065', '17/07/2017', 'AHMAD SAIFUR ROHMAN', 'XI RPL 1', 'Tanggul Wetan', '-', '-', '-', '-'),
('12785818065', '17/07/2017', 'AHMAD SOLIKIN', 'XI RPL 1', 'Batu Urip', '-', '-', '-', '-'),
('12786819065', '17/07/2017', 'AJENG FARENZA ANGGRAENI', 'XI RPL 1', 'Tanggul ', '-', '-', '-', '-'),
('12787820065', '17/07/2017', 'ALVANO ADITYA FRISANDI', 'XI RPL 1', 'Teko\'an', '-', '-', '-', '-'),
('12788821065', '17/07/2017', 'ALVITA ZERLINA GRANTIANA', 'XI RPL 1', 'Wringin Agung', '-', '-', '-', '-'),
('12789822065', '17/07/2017', 'AMYRA SISKA', 'XI RPL 1', 'Sumber Agung', '-', '-', '-', '-'),
('12790823065', '17/07/2017', 'ANIS MARCELA', 'XI RPL 1', 'Patemon', '-', '-', '-', '-'),
('12791824065', '17/07/2017', 'ANTIN DWI ROUDHATUL JANNAH', 'XI RPL 1', 'Pondok Dalem', '-', '-', '-', '-'),
('12792825065', '17/07/2017', 'BUDI WIBOWO', 'XI RPL 1', 'Manggisan', '-', '-', '-', '-'),
('12793826065', '17/07/2017', 'DANI YUSUF', 'XI RPL 1', 'Sidomulyo', '-', '-', '-', '-'),
('12794827065', '17/07/2017', 'DEBY INDRA WONO', 'XI RPL 1', 'Curah Bamban', '-', '-', '-', '-'),
('12795828065', '17/07/2017', 'DEWI FIRAWATI', 'XI RPL 1', 'Teko\'an', '-', '-', '-', '-'),
('12796829065', '17/07/2017', 'DEWI INTAN MASIATI', 'XI RPL 1', 'Bangsalsari', '-', '-', '-', '-'),
('12797830065', '17/07/2017', 'DHIEN MIVTHA FAHIRA LINAR BASUKI', 'XI RPL 1', 'Klatakan', '-', '-', '-', '-'),
('12798831065', '17/07/2017', 'DICKY INDRA SETIAWAN', 'XI RPL 1', 'Tanggul', '-', '-', '-', '-'),
('12799832065', '17/07/2017', 'DIDIN SETIANDARI', 'XI RPL 1', 'Pondok Rampal', '-', '-', '-', '-'),
('12800833065', '17/07/2017', 'EKO ADI PRASETYO', 'XI RPL 1', 'Rowo Tengah', '-', '-', '-', '-'),
('12801834065', '17/07/2017', 'ELVIANA DHAMAYANTI', 'XI RPL 1', 'Sumber Agung', '-', '-', '-', '-'),
('12802835065', '17/07/2017', 'FAKTA FILA SAFIRA', 'XI RPL 1', 'Batu Urip', '-', '-', '-', '-'),
('12803836065', '17/07/2017', 'FARHAN QOLBI', 'XI RPL 1', 'Klatakan', '-', '-', '-', '-'),
('12804837065', '17/07/2017', 'FARHAN TRI KURNIAWAN', 'XI RPL 1', 'Lampung Kuning', '-', '-', '-', '-'),
('12805838065', '17/07/2017', 'GALIH FAJAR RAMADON', 'XI RPL 1', 'Kencong', '-', '-', '-', '-'),
('12806839065', '17/07/2017', 'GALIH KUSUMA MUSLIM', 'XI RPL 1', 'Curah Bamban', '-', '-', '-', '-'),
('12807840065', '17/07/2017', 'HAFID', 'XI RPL 1', 'Gembongan', '-', '-', '-', '-'),
('12808841065', '17/07/2017', 'IZZA SONA TAMAR SARASYEH', 'XI RPL 1', 'Tancak Tulis', '-', '-', '-', '-'),
('12809842065', '17/07/2017', 'JAKA HAIKHAL  WAFI', 'XI RPL 1', 'Bangsalsari', '-', '-', '-', '-'),
('12810843065', '17/07/2017', 'KHAFIFAH OKTA VIANI A.', 'XI RPL 1', 'Pondok Dalem', '-', '-', '-', '-'),
('12811844065', '17/07/2017', 'LAILATUL KARIMAH', 'XI RPL 1', 'Pondok Dalem', '-', '-', '-', '-'),
('12812845065', '17/07/2017', 'LIAN MAULIDA', 'XI RPL 1', 'Bangsalsari', '-', '-', '-', '-'),
('12813846065', '17/07/2017', 'LINDA DWI WARDANI ROHMAH', 'XI RPL 1', 'Patemon', '-', '-', '-', '-'),
('12814847065', '17/07/2017', 'LINTANG PURNAMA NINGSIH ATIK', 'XI RPL 2', 'Manggisan', '-', '-', '-', '-'),
('12815848065', '17/07/2017', 'M. YUSA IKHSAN', 'XI RPL 2', 'Jl. Durian', '-', '-', '-', '-'),
('12816849065', '17/07/2017', 'MAULIDANI BALKIS YUGO ARIUZ', 'XI RPL 2', 'Bangsalsari', '-', '-', '-', '-'),
('12817850065', '17/07/2017', 'MAYA NURUL SAFITRI', 'XI RPL 2', 'Teko\'an', '-', '-', '-', '-'),
('12818851065', '17/07/2017', 'MOCH. ADI FIRMANSYAH', 'XI RPL 2', 'Curah Bamban', '-', '-', '-', '-'),
('12819852065', '17/07/2017', 'MOH. KUSNADI', 'XI RPL 2', 'Gambirono', '-', '-', '-', '-'),
('12820853065', '17/07/2017', 'MOHAMMAD AL FARIZI MASRIQI', 'XI RPL 2', 'Jatiroto', '-', '-', '-', '-'),
('12821854065', '17/07/2017', 'MUHAMMAD AUWIBI', 'XI RPL 2', 'Manggisan', '-', '-', '-', '-'),
('12822855065', '17/07/2017', 'MUHAMMAD GUFRON ARDIANSYAH', 'XI RPL 2', 'Klatakan', '-', '-', '-', '-'),
('12823856065', '17/07/2017', 'MUHAMMAD I`LAL SARIFUDIN', 'XI RPL 2', 'Umbulsari', '-', '-', '-', '-'),
('12824857065', '17/07/2017', 'MUHAMMAD RIDWAN AGUSTA', 'XI RPL 2', 'Karang Bayat', '-', '-', '-', '-'),
('12825858065', '17/07/2017', 'MUHAMMAD SHOLEH', 'XI RPL 2', 'Curah Bamban', '-', '-', '-', '-'),
('12826859065', '17/07/2017', 'MUHAMMAD YASIN', 'XI RPL 2', 'Patemon', '-', '-', '-', '-'),
('12827860065', '17/07/2017', 'NUR AYU SUSILA', 'XI RPL 2', 'Pondok Dalem', '-', '-', '-', '-'),
('12828861065', '17/07/2017', 'NUR AZIZAH', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12829862065', '17/07/2017', 'NURUL HIDAYANTI', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12830863065', '17/07/2017', 'PRATAMA UTAMA SANJAYA', 'XI RPL 2', 'Wonorejo Kencong', '-', '-', '-', '-'),
('12831864065', '17/07/2017', 'PRAYOGO PANGESTU', 'XI RPL 2', 'POM Bensin Tanggul', '-', '-', '-', '-'),
('12832865065', '17/07/2017', 'PUTU DEA MANGGARANY NUANDRA RATU', 'XI RPL 2', 'Teko\'an Depan POM Bensin', '-', '-', '-', '-'),
('12833866065', '17/07/2017', 'RESTU SETIA ADIPUTRA', 'XI RPL 2', 'Bangsalsari', '-', '-', '-', '-'),
('12834867065', '17/07/2017', 'RINA MARSELA', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12835868065', '17/07/2017', 'RINAWATI SEKAR WANGI', 'XI RPL 2', 'Pondok Dalem', '-', '-', '-', '-'),
('12836869065', '17/07/2017', 'RIZAL FEBRIAN', 'XI RPL 2', 'Bangsalsari', '-', '-', '-', '-'),
('12837870065', '17/07/2017', 'RIZKI MAYA NAZURAH', 'XI RPL 2', 'Teko\'an', '-', '-', '-', '-'),
('12838871065', '17/07/2017', 'RONALD ROBI SUPANDI', 'XI RPL 2', 'Mayang', '-', '-', '-', '-'),
('12839872065', '17/07/2017', 'ROY HANUL ULUM JASMI', 'XI RPL 2', 'Manggisan', '-', '-', '-', '-'),
('12840873065', '17/07/2017', 'RYAN IRAWAN', 'XI RPL 2', 'Sukorno', '-', '-', '-', '-'),
('12841874065', '17/07/2017', 'SINTA BELLA CRISKA', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12842875065', '17/07/2017', 'SITI KAMILA', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12843876065', '17/07/2017', 'SRI WAHYUNINGSIH', 'XI RPL 2', 'Manggisan', '-', '-', '-', '-'),
('12844877065', '17/07/2017', 'SYAHRUL GUNAWAN', 'XI RPL 2', 'Sumber Baru', '-', '-', '-', '-'),
('12845878065', '17/07/2017', 'VALENTSIH PUTRA WISUDANI', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12846879065', '17/07/2017', 'YOSHI NOBU', 'XI RPL 2', 'Sumber Baru', '-', '-', '-', '-'),
('12847880065', '17/07/2017', 'YULIAN FEBRIANSYAH', 'XI RPL 2', 'Gambirono', '-', '-', '-', '-'),
('12848881065', '17/07/2017', 'YUSRIL ALIM AINURROHIM', 'XI RPL 2', 'Sumber Agung', '-', '-', '-', '-'),
('12849212067', '17/07/2017', 'ABDUL QODIR', 'XI MM 1', 'Klatakan', '-', '-', '-', '-'),
('12850213067', '17/07/2017', 'ADE APRILIA', 'XI MM 1', 'Teko\'an', '-', '-', '-', '-'),
('12851214067', '17/07/2017', 'ADIB NABHAN MUSYAFFA`', 'XI MM 1', 'Rowo Tengah', '-', '-', '-', '-'),
('12852215067', '17/07/2017', 'AHMAD DENI WAHYUDI', 'XI MM 1', 'Karang Bayat', '-', '-', '-', '-'),
('12853216067', '17/07/2017', 'AHMAD KHOLIEL ZAINUR ROZIQIN', 'XI MM 1', 'Bangsalsari', '-', '-', '-', '-'),
('12854217067', '17/07/2017', 'AMALIYA NURIMAMA PANDUWINATA', 'XI MM 1', 'Tanggul Wetan', '-', '-', '-', '-'),
('12855218067', '17/07/2017', 'ANDI KURNIAWAN', 'XI MM 1', 'Tanggul Kulon', '-', '-', '-', '-'),
('12856219067', '17/07/2017', 'ANDRA BAYU EFENDI', 'XI MM 1', 'Sariono', '-', '-', '-', '-'),
('12857220067', '17/07/2017', 'ANDRIANA DAMAYANTI', 'XI MM 1', 'Bangsalsari', '-', '-', '-', '-'),
('12858221067', '17/07/2017', 'ANIK MELASANTI', 'XI MM 1', 'Sumberagung', '-', '-', '-', '-'),
('12859222067', '17/07/2017', 'ARIF KURNIAWAN', 'XI MM 1', 'Teko\'an', '-', '-', '-', '-'),
('12860223067', '17/07/2017', 'ASHADI', 'XI MM 1', 'Pucu\'an', '-', '-', '-', '-'),
('12861224067', '17/07/2017', 'AYU FEBRIANA  LESTARI', 'XI MM 1', 'Pondok Waluh', '-', '-', '-', '-'),
('12862225067', '17/07/2017', 'CANDRA MARCELLINA PERMADANI', 'XI MM 1', 'Curah Bamban', '-', '-', '-', '-'),
('12863226067', '17/07/2017', 'CHARISMA CAHAYA ANDHINI', 'XI MM 1', 'Rambipuji', '-', '-', '-', '-'),
('12864227067', '17/07/2017', 'DEWI USWATUN HASANAH', 'XI MM 1', 'Sadengan', '-', '-', '-', '-'),
('12865228067', '17/07/2017', 'DESY KHARISMA SARI', 'XI MM 1', 'Tanggul Wetan', '-', '-', '-', '-'),
('12866229067', '17/07/2017', 'DIAH HOYRUNIKA PRATIWI', 'XI MM 1', 'Curah Bamban', '-', '-', '-', '-'),
('12867230067', '17/07/2017', 'DIAN NOVI ANGGRAENI', 'XI MM 1', 'Pucu\'an', '-', '-', '-', '-'),
('12868231067', '17/07/2017', 'DIAN OKTAVIA ANGGRAINI', 'XI MM 1', 'Tanggul Wetan', '-', '-', '-', '-'),
('12869232067', '17/07/2017', 'DINDA NAVA DWITA AL FILLAH', 'XI MM 1', 'Rowo Tengah', '-', '-', '-', '-'),
('12870233067', '17/07/2017', 'DITA YUDIA', 'XI MM 1', 'Sumberejo', '-', '-', '-', '-'),
('12871234067', '17/07/2017', 'DORIS OKVITASARI', 'XI MM 1', 'Sidorejo', '-', '-', '-', '-'),
('12872235067', '17/07/2017', 'DUDION SUSANTO', 'XI MM 1', 'Sariono', '-', '-', '-', '-'),
('12873236067', '17/07/2017', 'DWI ERLINA', 'XI MM 1', 'Rowo Tengah', '-', '-', '-', '-'),
('12874237067', '17/07/2017', 'EDI SUPRIYANTO', 'XI MM 1', 'Karang Sono', '-', '-', '-', '-'),
('12875238067', '17/07/2017', 'ELI FATMAWATI', 'XI MM 1', 'Gambirono', '-', '-', '-', '-'),
('12876239067', '17/07/2017', 'FANI PUJIANTO', 'XI MM 1', 'Rowo Tengah', '-', '-', '-', '-'),
('12877240067', '17/07/2017', 'FERDI AULIYAUR ROHMAN', 'XI MM 1', 'Curah Bamban', '-', '-', '-', '-'),
('12878241067', '17/07/2017', 'FIFIN ADILA SEPTYA RAHAYU', 'XI MM 1', 'Umbulsari', '-', '-', '-', '-'),
('12879242067', '17/07/2017', 'FIKA SAFIRA', 'XI MM 1', 'Tambakrejo', '-', '-', '-', '-'),
('12880243067', '17/07/2017', 'FIQRI HAIKAL', 'XI MM 1', 'Klatakan', '-', '-', '-', '-'),
('12881244067', '17/07/2017', 'GITHA PRIMA TRI NABILA', 'XI MM 1', 'Gambirono', '-', '-', '-', '-'),
('12882245067', '17/07/2017', 'HARDIK GILANG DEO SAPUTRA', 'XI MM 1', 'Sumberagung', '-', '-', '-', '-'),
('12883246067', '17/07/2017', 'HENNI INDRIA NINGSIH', 'XI MM 1', 'Pondok Waluh', '-', '-', '-', '-'),
('12884247067', '17/07/2017', 'IDAM YUDHA DWICAKSONO', 'XI MM 2', 'Sumberbaru', '-', '-', '-', '-'),
('12885248067', '17/07/2017', 'IKA YULIANA SARI', 'XI MM 2', 'Sidomulyo', '-', '-', '-', '-'),
('12886249067', '17/07/2017', 'IVAN SOVFAN', 'XI MM 2', 'Karangbayat', '-', '-', '-', '-'),
('12887250067', '17/07/2017', 'JORDI HARSONO', 'XI MM 2', 'Sarimulyo', '-', '-', '-', '-'),
('12888251067', '17/07/2017', 'KAFIN HAKIKI', 'XI MM 2', 'Jatiroto Utara', '-', '-', '-', '-'),
('12889252067', '17/07/2017', 'KHOLIFATUS SYA`DIYAH', 'XI MM 2', 'Gembongan Tanggul Kulon', '-', '-', '-', '-'),
('12890253067', '17/07/2017', 'KRISDIANA MEGA KUSUMA NINGRUM', 'XI MM 2', 'Gembongan Tanggul Kulon', '-', '-', '-', '-'),
('12891254067', '17/07/2017', 'LISA NATALIA', 'XI MM 2', 'Darungan', '-', '-', '-', '-'),
('12892255067', '17/07/2017', 'LUKMAN SAHRONI', 'XI MM 2', 'Bangsalsari', '-', '-', '-', '-'),
('12893256067', '17/07/2017', 'MARISA PUTRI', 'XI MM 2', 'Manggisan', '-', '-', '-', '-'),
('12894257067', '17/07/2017', 'MASLINA WATUL WAZRIYAH', 'XI MM 2', 'Rowotengah', '-', '-', '-', '-'),
('12895258067', '17/07/2017', 'MOCH.  ILHAM YUNIAR PRADITAMA', 'XI MM 2', 'Sariono', '-', '-', '-', '-'),
('12896259067', '17/07/2017', 'MOCH. FAHRUL FIRMANSYAH', 'XI MM 2', 'Gumukmas', '-', '-', '-', '-'),
('12897260067', '17/07/2017', 'MOH. ARUM AKHSON', 'XI MM 2', 'Kramat Sukoharjo', '-', '-', '-', '-'),
('12898261067', '17/07/2017', 'MUHAMMAD KHOJINUL ASROR', 'XI MM 2', 'Gembongan Tanggul Kulon', '-', '-', '-', '-'),
('12899262067', '17/07/2017', 'MUHAMMAD SIROJUL MUNIR', 'XI MM 2', 'Tayeng', '-', '-', '-', '-'),
('12900263067', '17/07/2017', 'MUHAMMAD TAUFIK HIDAYATULLOH', 'XI MM 2', 'Klatakan', '-', '-', '-', '-'),
('12901264067', '17/07/2017', 'NANDA DWI PRASETYO', 'XI MM 2', 'Sumberbaru', '-', '-', '-', '-'),
('12902265067', '17/07/2017', 'NIA FADILA', 'XI MM 2', 'Tanggul Kulon', '-', '-', '-', '-'),
('12903266067', '17/07/2017', 'NOVIANA ERWIN DAMAYANTI', 'XI MM 2', 'Bangsalsari', '-', '-', '-', '-'),
('12904267067', '17/07/2017', 'NUR AFIFAH', 'XI MM 2', 'Tanggul Kulon', '-', '-', '-', '-'),
('12905268067', '17/07/2017', 'NUR ROFIQ HIDAYAT', 'XI MM 2', 'Karangbayat', '-', '-', '-', '-'),
('12906269067', '17/07/2017', 'NUR ROHMAN KHULAFAUR R', 'XI MM 2', 'Batu Urip', '-', '-', '-', '-'),
('12907270067', '17/07/2017', 'PUTRI AULIA KHARISMA AYU', 'XI MM 2', 'Pondok Waluh', '-', '-', '-', '-'),
('12908271067', '17/07/2017', 'PUTRI IKA MOVIDATUL JANNAH WIJIANTO', 'XI MM 2', 'Sumberbaru', '-', '-', '-', '-'),
('12909272067', '17/07/2017', 'PUTRI NOFA ANDRIATI', 'XI MM 2', '?', '-', '-', '-', '-'),
('12910273067', '17/07/2017', 'QURROTUL A`YUN', 'XI MM 2', 'Gambirono', '-', '-', '-', '-'),
('12911274067', '17/07/2017', 'SILVIA DWI SEPTIA', 'XI MM 2', 'Gembongan Tanggul Kulon', '-', '-', '-', '-'),
('12912275067', '17/07/2017', 'SUKARINI MUTIARA PUTRI', 'XI MM 2', 'Pondok Jeruk', '-', '-', '-', '-'),
('12913276067', '17/07/2017', 'ULIN NIKMAH EKA DIANITA', 'XI MM 2', 'Gembongan Tanggul Kulon', '-', '-', '-', '-'),
('12914277067', '17/07/2017', 'VICKY RIAN PRAYITNO', 'XI MM 2', 'Jatiroto', '-', '-', '-', '-'),
('12915278067', '17/07/2017', 'VIRA ANASTASYA', 'XI MM 2', 'Umburejo Semboro', '-', '-', '-', '-'),
('12916279067', '17/07/2017', 'WILDAN RACHMADONI', 'XI MM 2', 'Tanggul Kulon', '-', '-', '-', '-'),
('12917280067', '17/07/2017', 'WINDA BUDI LESTARI', 'XI MM 2', 'Sumberbaru', '-', '-', '-', '-'),
('12918281067', '17/07/2017', 'YULIANA SRI ASTUTIK', 'XI MM 2', 'Sariono', '-', '-', '-', '-'),
('12919282067', '17/07/2017', 'YUSRIL MAHENDRA PUTRA', 'XI MM 2', 'Tanggul Wetan', '-', '-', '-', '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`kode_buku`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gpeminjam`
--
ALTER TABLE `gpeminjam`
  ADD PRIMARY KEY (`id_gpinjam`);

--
-- Indexes for table `gpengunjung`
--
ALTER TABLE `gpengunjung`
  ADD PRIMARY KEY (`id_gpengunjung`),
  ADD KEY `no_anggota` (`no_anggota`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`no_anggota`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kejur`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjam`
--
ALTER TABLE `peminjam`
  ADD PRIMARY KEY (`id_pinjam`);

--
-- Indexes for table `pengunjung`
--
ALTER TABLE `pengunjung`
  ADD PRIMARY KEY (`id_pengunjung`);

--
-- Indexes for table `perpus`
--
ALTER TABLE `perpus`
  ADD PRIMARY KEY (`id_perpus`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`kode_rak`),
  ADD KEY `id_perpus` (`id_perpus`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `kode_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gpeminjam`
--
ALTER TABLE `gpeminjam`
  MODIFY `id_gpinjam` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gpengunjung`
--
ALTER TABLE `gpengunjung`
  MODIFY `id_gpengunjung` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `peminjam`
--
ALTER TABLE `peminjam`
  MODIFY `id_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pengunjung`
--
ALTER TABLE `pengunjung`
  MODIFY `id_pengunjung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
