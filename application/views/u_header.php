<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <title>Perpustakaan SMKN 6 Jember</title>
  <link rel="shortcut icon" href="<?php echo base_url('assets_2/img/smkn6jember.png')?>">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   
  <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

  <!-- Bootstrap -->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets_2/css/bootstrap.min.css')?>" />

  <!-- Slick -->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets_2/css/slick.css')?>" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets_2/css/slick-theme.css')?>" />

  <!-- nouislider -->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets_2/css/nouislider.min.css')?>" />

  <!-- Font Awesome Icon -->
  <link rel="stylesheet" href="<?php echo base_url('assets_2/css/font-awesome.min.css')?>">

  <!-- Custom stlylesheet -->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets_2/css/style.css')?>" />

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
  .pandek {
    width: 200px; 
    white-space: nowrap; 
    overflow: hidden;
    text-overflow: ellipsis;
    border: 0px solid lightgrey;
    padding: 5px;
    margin: 5px;
}
</style>
  <!-- Menu sort -->
  <style>

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 90px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown a:hover {background-color: #ddd;}

.show {display: block;}
</style>
  <!-- /menu sort -->
</head>

<body>
  <!-- HEADER -->
  <header>
    <!-- top Header -->
  
    <!-- /top Header -->

    <!-- header -->
    <div id="header">
      <div class="container">
        <div class="pull-left">
          <!-- Logo -->
          <div class="header-logo">
            <a class="logo" href="#">
             <img src="<?php echo base_url('assets_2/img/smk.png')?>" alt="">
            </a>
          </div>
          <!-- /Logo -->

          
        </div>
        <div class="pull-right">
          <ul class="header-btns">
            
            <!-- Search -->
          <div class="header-search">
           <form action="<?php echo base_url('buku/hasil')?>" action="GET">
              <table>
                <tr>
                  
                  <td width="300px"><input class="input"  type="text" placeholder="Masukkan Judul"  id="cari" name="cari"></td>

                  <td><button class="input" type="submit"><i class="fa fa-search"></i></button></td>
                  
                </tr>
              </table>
            </form>


      
          </div>
          <!-- /Search -->

            <!-- Mobile nav toggle-->
            <li class="nav-toggle">
              <button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
            </li>
            <!-- / Mobile nav toggle -->
          </ul>
        </div>
      </div>
      <!-- header -->
    </div>
    <!-- container -->
  </header>
  <!-- /HEADER -->

  <!-- NAVIGATION -->
  <div id="navigation">
    <!-- container -->
    <div class="container">
      <div id="responsive-nav">
        <!-- category nav -->
        
        <!-- /category nav -->

        <!-- menu nav -->
        <div class="menu-nav">
          <span class="menu-header">Menu <i class="fa fa-bars"></i></span>
          <ul class="menu-list">
            
            <li><a href="<?php echo base_url('buku/')?>">HOME</a></li>
            <li><a href="<?php echo base_url('buku/katalog/')?>">KATALOG</a></li>   
            <li><a href="<?php echo base_url('buku/kontak/')?>">KONTAK</a></li>
            <li><a href="<?php echo base_url('buku/event/')?>">EVENT</a></li>       
            
          </ul>
        </div>
        <!-- menu nav -->
      </div>
    </div>
    <!-- /container -->
  </div>
  <!-- /NAVIGATION -->