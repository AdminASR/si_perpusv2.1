
<div class="row">
  <div class="col-md-8">
    <div class="box">
    <div class="box-header">
              <h3 class="box-title"><i class="fa fa-envelope"></i> Komentar Pengunjung </h3>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>



                <tr>
                  <th>Tanggal</th>
                  <th>Komentar</th>
                  <th>aksi</th>
                </tr>
                </thead>
                <tbody>

                 <?php 
  
    foreach($komentar as $data){ 
    ?>
    <tr>
     <td><?php echo $data->tanggal ?></td>
      <td><?php echo $data->komentar ?></td>
     <td> <a href="<?php echo base_url('komentar/hapus/'.$data->id); ?>" class="btn btn-danger"><i class="fa fa-trash"></i>  Hapus</a></td>
    </tr>
    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Tanggal</th>
                  <th>Komentar</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-4">
    <div class="col-md-12">
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fa fa-battery-full"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Sangat baik</span>
              <div class="progress">
              </div>
              <span class="info-box-number"><?php echo $sb ?></span>
            </div>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-battery-three-quarters"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Baik</span>
              <div class="progress">
              </div>
              <span class="info-box-number"><?php echo $b ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-battery-2"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Cukup</span>
              <div class="progress">
              </div>
              <span class="info-box-number"><?php echo $c ?></span>
            </div>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="info-box bg-orange">
            <span class="info-box-icon"><i class="fa fa-battery-1"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Kurang Baik</span>
              <div class="progress">
              </div>
              <span class="info-box-number"><?php echo $j ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-battery-empty"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Buruk</span>
              <div class="progress">
              </div>
              <span class="info-box-number"><?php echo $sj ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
    
  </div>
  
</div>
      
