

<div class="row">
  <div class="col-md-7">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Buku Perpustakaan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kode</th>
                  <th>Judul buku</th>
                  <th>Pengarang</th>
                  <th>Kategori</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>


                  <?php

                  if(! empty($buku)){

                     foreach ($buku as $data) {
                        # code...
                      ?>
                <tr>
                  <td><?php echo $data->kode_buku ?></td>
                  <td><?php echo $data->judul ?></td>
                  <td><?php echo $data->penerbit ?></td>
                  <td><?php echo $data->kode_kategori ?></td>
                  <td align="center">
                    <?php if ($data->status=='1') { ?>
                      <form action="<?php echo base_url('rekomendasi/update_y/')?>" method="post">
                      <input type="hidden" name="kode_buku" value="<?php echo $data->kode_buku?>">
                      <input type="hidden" name="status" >
<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-star"></i></button>
                    </form>
                   <?php }elseif ($data->status=='0') {?>
                       <form action="<?php echo base_url('rekomendasi/update_y/')?>" method="post">
                      <input type="hidden" name="kode_buku" value="<?php echo $data->kode_buku?>">
                      <input type="hidden" name="status" >
<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Pilih</button>
                    </form>
                   <?php } ?>


                   
                  </td>
                </tr>
              <?php } } ?>
                </tbody>
                <tfoot>
               <tr>
                  <th>Kode</th>
                  <th>Judul buku</th>
                  <th>Pengarang</th>
                  <th>Kategori</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
  </div>
  

  <div class="col-md-5">
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Buku Yang Direkomendasikan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
               <tr>
                  <th>Kode</th>
                  <th>Judul buku</th>
                  <th>Kategori</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
               <tbody>


                  <?php

                  if(! empty($rkmnds_y)){

                     foreach ($rkmnds_y as $data) {
                        # code...
                      ?>
                     
                <tr>
                  <td><?php echo $data->kode_buku ?></td>
                  <td><?php echo $data->judul ?></td>
                 
                  <td><?php echo $data->kode_kategori ?></td>
                   <td>
                    <form action="<?php echo base_url('rekomendasi/update_b/')?>" method="post">
                      <input type="hidden" name="kode_buku" value="<?php echo $data->kode_buku?>">
                      <input type="hidden" name="status" >
<button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</button>
                    </form>
                  </td>
                </tr>
              <?php } }else {?>
           <tr>
             <td colspan="5" align="center">Tidak Ada Buku yang direkomendasikan</td>
           </tr>
        <?php } ?>
          </tbody>
                <tfoot>
               <tr>
                  <th>Kode</th>
                  <th>Judul buku</th>
                  <th>Kategori</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
  </div>
  

