

<div class="row">
        <div class="col-sm-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambahkan event</h3>
            </div>
<div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
      <form class="form-horizontal" action="<?php echo base_url('event/tambah_event')?>" enctype="multipart/form-data" method="post">
          <div class="col-md-1"></div><div class="col-md-5">
              <div class="box-body">
                 <div class="form-group">
                  <label>Tanggal</label>
                  <input type="date" class="form-control" name="tanggal">
                </div>
                <div class="form-group">
                  <label>Judul</label>
                  <input type="text" class="form-control" name="judul" placeholder="Judul">
                </div>
              </div>
          </div>
          <div class="col-md-1"></div><div class="col-md-5">
              <div class="box-body">
                  <label>Deskripsi</label>
                  <textarea class="form-control" rows="4" placeholder="Deskripsi" name="deskripsi" ></textarea>
              </div>
              <div class="box-body">
                 <label>Gambar</label>
              <input type="file" name="gambar_event">
              <input type="hidden" class="form-control" name="status" value="0">
              <p class="help-block">img/jpg/png</p>
              </div>
             
          </div>
            <div class="box-footer">
                <input type="submit" name="submit" value="Simpan" class="btn btn-primary pull-right ">
            </div>
        </form>


      
          </div>
        </div>
        </div>
      <div class="row">
  <div class="col-md-12">
    <div class="box">
    <div class="box-header">
              <h3 class="box-title">Daftar Event</h3>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Tanggal</th>
                  <th>Judul</th>             
                  <td align="center" colspan="2"><b>Aksi</b></td>
                </tr>
                </thead>
                <tbody>

                 <?php if (!empty($event)) {
                   # code...
                
                 $no = 1;
  
    foreach($event as $data){ 
    ?>
    <tr>
      <td><?php echo $no++ ?></td>
     <td><?php echo $data->tanggal ?></td>
      <td><?php echo $data->judul ?></td>
   
      <td align="center">
                    <?php if ($data->status=='1') { ?>
                      <form action="<?php echo base_url('event/update_b/')?>" method="post">
                      <input type="hidden" name="id" value="<?php echo $data->id?>">
                      <input type="hidden" name="status" >
                        <button type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Batal</button>
                    </form>
                   <?php }elseif ($data->status=='0') {?>
                       <form action="<?php echo base_url('event/update_y/')?>" method="post">
                      <input type="hidden" name="id" value="<?php echo $data->id?>">
                      <input type="hidden" name="status" >
                        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> Jadikan Utama</button>
                    </form>
                   <?php } ?>
                  </td>
                  <td> 
                    <a  href="<?=base_url()?>index.php/event/hapus/<?=$data->id?>/<?=$data->gambar?>" class="btn btn-danger"><i class="fa fa-trash"> Hapus</i></a>
                  </td>
            </tr>
         <?php } }else{  ?>
          <tr>
            <td colspan="4" align="center">Tidak Ada Event</td>
          </tr>
         <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Tanggal</th>
                  <th>Judul</th>             
                  <td align="center" colspan="2"><b>Aksi</b></td>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
  </div>
