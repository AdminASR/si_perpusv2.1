 <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php
              $sg=$jumlah1+$jumlah5;
              echo $sg; ?></h3>

              <p>Pengunjung Hari Ini</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="<?php echo base_url('laporan/Lpengunjung')?>" class="small-box-footer">selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $jumlah2 ?></h3>

              <p>Peminjam</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-plus"></i>
            </div>
            <a href="<?php echo base_url('laporan/Lpeminjaman')?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $jumlah3?></h3>

              <p>Jumlah Buku</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="<?php echo base_url('master/buku')?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $jumlah41 ?></h3>

              <p>Buku Yang di Pinjam</p>
            </div>
            <div class="icon">
              <i class="fa fa-bookmark"></i>
            </div>
            <a href="<?php echo base_url('laporan/Lpeminjaman')?>" class="small-box-footer">selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#guru" data-toggle="tab"><button class="btn btn-info">Guru</button></a></li>
              <li><a href="#siswa" data-toggle="tab"><button class="btn btn-primary">Siswa</button></a></li>
              <li class="pull-left header"><i class="fa fa-user"></i>Pengunjung</li>
            </ul>
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="siswa">
              <div class="box-body">
              <form role="form" method="post" action="<?php echo base_url('home/tambah_pengunjung')?>">
              <div class="box-body">
                 <div class="form-group">
                  <label for="exampleInputPassword1">Cari Nama Siswa </label> <a href="<?php echo base_url('master/form_tambah_siswa')?>"> Belum terdaftar?</a><br> Nama (Nis)
                  <input type='text'
                                  required="" 
                                   placeholder="--nama siswa--"
                                   class='flexdatalist form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='languages'
                                   name='nis'>

                            <datalist id="languages">
                              <?php 
                              foreach($siswa as $u){ 
                              ?>
                                <option><?php echo $u->nama_siswa?> (<?php echo $u->nis?>)</option>
                              <?php }?>
                            </datalist>
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Keterangan</label><br>
                  <input type="hidden" class="form-control" name="tanggal" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date('Y-m-d H:i:s')?>" >
                    <input type="radio"  name="keterangan" value="Membaca buku" required="" >Membaca Buku 
                    <input type="radio"  name="keterangan" value="Meminjam buku" required="" >Meminjam Buku 
                    <input type="radio"  name="keterangan" value="Mengembalikan buku" required="" >Mengembalikan Buku
                </div>
              </div>
              <div class="box-footer">
                <button type="submit"  class="btn btn-success pull-right">Tambahkan</button>
              </div>
            </form>
            </div>
              </div>
              <div class="chart tab-pane" id="guru">
                <div class="box-body">
              <form role="form" method="post" action="<?php echo base_url('home/tambah_gpengunjung')?>">
              <div class="box-body">
                 <div class="form-group">
                  <label for="exampleInputPassword1">Cari Nama Guru</label> <a href="<?php echo base_url('master/form_tambah_guru')?>"> Belum terdaftar?</a><br> Nama (No Anggota)
                  <input type='text'
                                  required="" 
                                   placeholder="--nama guru--"
                                   class='flexdatalist1 form-control'
                                   data-min-length='1'
                                   data-selection-required='true'
                                   list='guru'
                                   name='no_anggota'>

                            <datalist id="guru">
                              <?php 
                              foreach($guru as $g){ 
                              ?>
                                <option><?php echo $g->nama ?> (<?php echo $g->no_anggota ?>)</option>
                              <?php }?>
                            </datalist>
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Keterangan</label><br>
                  <input type="hidden" class="form-control" name="tanggal" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date('Y-m-d H:i:s')?>" >
                    <input type="radio"  name="keterangan" value="Membaca buku" required="" >Membaca Buku 
                    <input type="radio"  name="keterangan" value="Meminjam buku" required="" >Meminjam Buku 
                    <input type="radio"  name="keterangan" value="Mengembalikan buku" required="" >Mengembalikan Buku
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Tambahkan</button>
              </div>
            </form>
            </div>
              </div>
            </div>
          </div>
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-6 connectedSortable">
            <div class="callout callout-info">
              <h3>PERPUSTAKAAN SMKN 6 JEMBER</h3>
              Halaman ini hanya dapat diakses oleh admin dimana seorang admin dapat melakukan kegiatan-kegiatan yang bersangkutan dengan administrasi yaitu sebagai berikut:<br>
              <ul><li>Mengelola Data Siswa,<br></li>
              <li>Mengelola Data Buku,<br></li>
              <li>Mengelola Data Pengunjung Harian,<br></li>
              <li>Mengelola Laporan Pengunjung Dan Peminjaman,<br></li>
              <li>Melakukan Transaksi Baik Peminjaman maupun pengembalian,<br></li>
              <li>Menambahkan Event,<br></li>
              <li>Dan Merekomendasikan Buku Kepada Pengunjung.<br></li></ul>
            </div>
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

      <div class="row">
      <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#tguru" data-toggle="tab"><button class="btn btn-info">Guru</button></a></li>
              <li><a href="#tsiswa" data-toggle="tab"><button class="btn btn-primary">Siswa</button></a></li>
              <li class="pull-left header">
              <h3 class="box-title">Data Pengunjung hari ini : 
              <?php
              $hari=date("D");
              if ($hari=='Sun') {
                echo "Minggu";
              }elseif ($hari=='Mon') {
                echo "Senin";              
              }elseif ($hari=='Tue') {
                echo "Selasa";              
              }elseif ($hari=='Wed') {
                echo "Rabu";              
              }elseif ($hari=='Thu') {
                echo "Kamis";              
              }elseif ($hari=='Fri') {
                echo "Jumat";              
              }elseif ($hari=='Sat') {
                echo "Sabtu";              
              }
              ?>,
              <?php echo date("d-m-Y")?></h3></li>
            </ul>
            <!-- Tabs within a box -->
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="tsiswa">
              <div class="box-body">
              <div class="box">
            <div class="box-header">
              <h3 class="box-title">Siswa </h3>
              <a href="<?php echo base_url('laporan/Lpengunjung')?>"><button class="btn btn-info pull-right"> Selengkapnya > </button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table width="100%" id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($pengunjung as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td > <?php
                  $jam=substr($p->tanggal, 11);
                  echo $jam;
                  ?> </td>
                  <td><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->id_kejur?></td>
                  <td><?php echo $p->keterangan?></td>
                  <td>
                    <center><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-info"  onclick="tampildata('<?php echo $p->id_pengunjung ?>','<?php echo $p->nama_siswa ?>','<?php echo $p->id_kejur ?>','<?php echo $p->keterangan ?>')"><i class="fa fa-edit"></i> edit</button> 
                  <?php echo anchor('home/hapushs/'.$p->id_pengunjung,'<button onclick="hapus()" class="btn btn-danger"><i class="fa fa-trash"></i>Hapus</button>'); ?></center>
                </td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
              </div>
              <div class="chart tab-pane" id="tguru">
                <div class="box-body">
              <div class="box">
            <div class="box-header">
              <h3 class="box-title">Guru </h3>
              <a href="<?php echo base_url('laporan/Lpengunjung')?>"><button class="btn btn-info pull-right"> Selengkapnya > </button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table width="100%" id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($gpengunjung as $g){ 
                ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td > <?php
                  $jam=substr($g->tanggal, 11);
                  echo $jam;
                  ?> </td>
                  <td><?php echo $g->nama?></td>
                  <td><?php echo $g->jabatan?></td>
                  <td><?php echo $g->keterangan?></td>
                  <td>
                    <center><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-info2"  onclick="tampildata2('<?php echo $g->id_gpengunjung ?>','<?php echo $g->nama ?>','<?php echo $g->jabatan ?>','<?php echo $g->keterangan ?>')"><i class="fa fa-edit"></i> edit</button> 
                  <?php echo anchor('home/hapushg/'.$g->id_gpengunjung,'<button onclick="hapus()" class="btn btn-danger"><i class="fa fa-trash"></i>Hapus</button>'); ?></center>
                </td>
                </tr>
              <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Keterangan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
              </div>
            </div>
          </div>
        </section>
      </div>


          <div class="modal modal-default fade" id="modal-info" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Keterangan Pengunjung</h4>
              </div>
              <div class="modal-body box-header">
              
<div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url(). 'home/updatehs'; ?>" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3">Nama Siswa</label>

                  <div class="col-sm-8">
                <input type="hidden" class="form-control" name="id_pengunjung" id="s">
                    <input type="text" readonly="" class="form-control" name="nama_siswa" id="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kelas/Jurusan</label>

                  <div class="col-sm-8">
                    <input type="text" readonly="" class="form-control" name="id_kejur" id="kejur">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Keterangan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="keterangan" id="ket">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
              </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>


      <div class="modal modal-default fade" id="modal-info2" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Keterangan Pengunjung</h4>
              </div>
              <div class="modal-body box-header">
              
<div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" action="<?php echo base_url(). 'home/updatehg'; ?>" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-3">Nama Guru</label>

                  <div class="col-sm-8">
                <input type="hidden" class="form-control" name="id_gpengunjung" id="g">
                    <input type="text" readonly="" class="form-control" name="nama" id="namag">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jabatan</label>

                  <div class="col-sm-8">
                    <input type="text" readonly="" class="form-control" name="jabatan" id="jabatan">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Keterangan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="keterangan" id="ketg">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
              </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>
        <!-- /.modal -->
 <script>
    function tampildata( id_pengunjung, nama_siswa, id_kejur, keterangan){
      $('#nama').val(nama_siswa);
      $('#kejur').val(id_kejur);
      $('#ket').val(keterangan);
      $('#s').val(id_pengunjung);
      
    }
    function tampildata2( id_gpengunjung, nama, jabatan, keterangan){
      $('#namag').val(nama);
      $('#jabatan').val(jabatan);
      $('#ketg').val(keterangan);
      $('#g').val(id_gpengunjung);
      
    }
  </script>
        