<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Perpustakaan</title>
     <link rel="shortcut icon" href="<?php echo base_url('assets/dist/img/smkn6jember.jpg')?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css')?>">
  <link  rel="stylesheet" type="text/css" href="<?php echo base_url('assets/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.css')?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file-text:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
          <style>
  .pandek {
    width: 200px; 
    white-space: nowrap; 
    overflow: hidden;
    text-overflow: ellipsis;
    border: 0px solid lightgrey;
    padding: 5px;
    margin: 5px;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Per</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Perpustakaan</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php echo $notif2?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><?php echo $notif2?> Pesan baru</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php foreach ($notif as $n) {
                    ?>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> <b><?php echo $n->nama_siswa?>(<?php echo $n->id_kejur?>) </b>,Judul : <?php echo $n->judul?>(<?php echo $n->jml?>)
                    </a>
                  </li>
                <?php } ?>
                </ul>
              </li>
              <li class="footer"><a href="<?php echo base_url('laporan/Lpeminjaman')?>">Lihat semua</a></li>
            </ul>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/dist/img/smkn6jember.jpg')?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata("user"); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/dist/img/smkn6jember.jpg')?>" class="img-circle" alt="User Image">

                <p><?php echo $this->session->userdata("user"); ?>
                  <small> SMK Negeri 6 Jember</small>
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('pegawai/edit_akun/'.$this->session->userdata("id_pegawai"))?>" class="btn btn-default btn-flat">Setting Akun</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/dist/img/smkn6jember.jpg')?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>SMKN 6 JEMBER</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('home')?>">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('peminjam')?>">
            <i class="fa fa-user-plus"></i> <span>Peminjaman</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('pengembalian')?>">
            <i class="fa fa-user-plus"></i> <span>Pengembalian</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('event')?>">
            <i class="fa fa-star"></i> <span>Event</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('rekomendasi')?>">
            <i class="fa fa-thumbs-o-up"></i> <span>Rekomendsi</span>
          </a>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('master/siswa')?>"><i class="fa fa-users"></i> Siswa</a></li>
            <li><a href="<?php echo base_url('master/guru')?>"><i class="fa fa-users"></i> Guru</a></li>
            <li><a href="<?php echo base_url('master/buku')?>"><i class="fa fa-book"></i> Buku</a></li>
            <?php if($this->session->userdata('jabatan')==='1'):?>
              <li><a href="<?php echo base_url('master/pegawai')?>"><i class="fa fa-user"></i> Pegawai</a></li>
            <?php endif;?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-users"></i> Pengunjung
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('laporan/pengunjung_harian')?>"><i class="fa fa-circle-o"></i> Harian</a></li>
                <li><a href="<?php echo base_url('laporan/pengunjung_bulanan')?>"><i class="fa fa-circle-o"></i> Bulanan</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-book"></i> Peminjaman
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url('laporan/peminjam_harian')?>"><i class="fa fa-circle-o"></i> Harian</a></li>
                <li><a href="<?php echo base_url('laporan/peminjam_bulanan')?>"><i class="fa fa-circle-o"></i> Bulanan</a></li>
              </ul>
            </li>
          </ul>
        </li>
        
          <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Cetak Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-dot-circle-o"></i> Pengunjung</a></li>
            <li><a href="#"><i class="fa fa-dot-circle-o"></i> Peminjaman</a></li>
            <li><a href="#"><i class="fa fa-dot-circle-o"></i> Pengembalian</a></li>
          </ul>
        </li> -->
         <li>
          <a href="<?php echo base_url('komentar')?>">
            <i class="fa fa-comments-o"></i> <span>Komentar</span>
          </a>
        </li>

        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">