<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Laporan Pengunjung Harian : <?php echo $tes?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="<?php echo base_url(). 'laporan/pengunjung_harian'; ?>">
                    <label>Tanggal : </label>
                     <input type="date" name="tanggal" required="">
                    <input type="submit" value="OK" class="btn bg-purple  btn-xs">
              </form>
              <form method="post" action="<?php echo base_url(). 'cetak/pengunjung_harian'; ?>">
                    <input type="hidden" name="tanggal" value="">
                    <button type="submit" class="btn bg-purple pull-right btn-sm"><i class="fa fa-print"></i> cetak</button>
              </form>
              <br><br>
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Kegiatan</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $n=1;
                foreach($pengunjung as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php $jam=substr($p->tanggal,11);
                  echo $jam;
                  ?></td>
                  <td><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->id_kejur?></td>
                  <td><?php echo $p->keterangan?></td>
                </tr>
                <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Waktu</th>
                  <th>Nama</th>
                  <th>Kelas</th>
                  <th>Kegiatan</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    