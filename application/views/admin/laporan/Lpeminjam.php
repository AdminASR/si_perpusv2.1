
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li><a href="#over" data-toggle="tab"><button class="btn btn-danger">over</button></a></li>
              <li><a href="#expired" data-toggle="tab"><button class="btn btn-warning">expired</button></a></li>
              <li><a href="#semua" data-toggle="tab"><button class="btn btn-info">All</button></a></li>
              <li class="pull-left header"><i class="fa fa-book"></i> Laporan Peminjaman</li>
              <li class="pull-left header"><button class="btn btn-primary"><i class="glyphicon glyphicon-print"></i> Cetak</button></li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="semua">
              <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Peminjam</th>
                  <th>Judul</th>
                  <th>Jumlah</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th>Status</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $n=1;
                $asr=date('Y-m-d');
                foreach($peminjam as $p){ 
                ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php echo $p->nama_siswa?></td>
                  <td><?php echo $p->judul?></td>
                  <td><?php echo $p->jml?></td>
                  <td><?php echo $p->tgl_pinjam?></td>
                  <td><?php echo $p->tgl_kembali?></td>
                  <td><?php if ($p->tgl_kembali>$asr) {
                    echo "<i class='fa fa-thumbs-o-up bg-green'></i>";
                  }else if ($p->tgl_kembali==$asr) {
                    echo "<i class='fa fa-warning bg-yellow'> </i>";
                  }else if ($p->tgl_kembali<$asr) {
                    echo "<i class='fa fa-remove bg-red'>  </i>";
                  }
                   ?></td>
                  <td><button class="btn btn-info"><i class="fa fa-file-text"></i> detail</button> <button class="btn btn-success"><i class="fa fa-edit"></i> edit</button> <button class="btn btn-danger"><i class="fa fa-trash"></i> hapus</button></td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                 <tr>
                  <th>No</th>
                  <th>Peminjam</th>
                  <th>Judul</th>
                  <th>Jumlah</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th>Status</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
              </div>
              <div class="chart tab-pane" id="expired">
                <div class="box-body">
              <table id="example4" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Peminjam</th>
                  <th>Judul</th>
                  <th>Jumlah</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th>Status</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $n=1;
                $asr=date('Y-m-d');
                foreach($peminjam1 as $p1){ 
                ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php echo $p1->nama_siswa?></td>
                  <td><?php echo $p1->judul?></td>
                  <td><?php echo $p1->jml?></td>
                  <td><?php echo $p1->tgl_pinjam?></td>
                  <td><?php echo $p1->tgl_kembali?></td>
                  <td><?php if ($p1->tgl_kembali>$asr) {
                    echo "<i class='fa fa-thumbs-o-up bg-green'></i>";
                  }else if ($p1->tgl_kembali==$asr) {
                    echo "<i class='fa fa-warning bg-yellow'></i>";
                  }else if ($p1->tgl_kembali<$asr) {
                    echo "<i class='fa fa-remove bg-red'></i>";
                  }
                   ?></td>
                  <td><button class="btn btn-info"><i class="fa fa-file-text"></i> detail</button> <button class="btn btn-success"><i class="fa fa-edit"></i> edit</button> <button class="btn btn-danger"><i class="fa fa-trash"></i> hapus</button></td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                 <tr>
                  <th>No</th>
                  <th>Peminjam</th>
                  <th>Judul</th>
                  <th>Jumlah</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th>Status</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
              </div>
              <div class="chart tab-pane" id="over">
                <div class="box-body">
              <table id="example3" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Peminjam</th>
                  <th>Judul</th>
                  <th>Jumlah</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th>Status</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $n=1;
                $asr=date('Y-m-d');
                foreach($peminjam2 as $p2){ 
                ?>
                <tr>
                  <td><?php echo $n++ ?></td>
                  <td><?php echo $p2->nama_siswa?></td>
                  <td><?php echo $p2->judul?></td>
                  <td><?php echo $p2->jml?></td>
                  <td><?php echo $p2->tgl_pinjam?></td>
                  <td><?php echo $p2->tgl_kembali?></td>
                  <td><?php if ($p2->tgl_kembali>$asr) {
                    echo "<i class='fa fa-thumbs-o-up bg-green'></i>";
                  }else if ($p2->tgl_kembali==$asr) {
                    echo "<i class='fa fa-warning bg-yellow'></i>";
                  }else if ($p2->tgl_kembali<$asr) {
                    echo "<i class='fa fa-remove bg-red'></i>";
                  }
                   ?></td>
                  <td><button class="btn btn-info"><i class="fa fa-file-text"></i> detail</button> <button class="btn btn-success"><i class="fa fa-edit"></i> edit</button> <button class="btn btn-danger"><i class="fa fa-trash"></i> hapus</button></td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                 <tr>
                  <th>No</th>
                  <th>Peminjam</th>
                  <th>Judul</th>
                  <th>Jumlah</th>
                  <th>tgl Pinjam</th>
                  <th>tgl Kembali</th>
                  <th>Status</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
              </div>
            </div>
          </div>
        </section>
      </div>

      