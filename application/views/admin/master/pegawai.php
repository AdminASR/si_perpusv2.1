
<div class="box">
    <div class="box-header">
              <h3 class="box-title"><i class="fa fa-users"></i> Petugas Perpustakaan</h3>
              <a href="<?php echo base_url('master/form_tambah_pegawai')?>"><button class="btn btn-success pull-right"><i class="fa fa-user-plus"></i> Tambah Data</button></a>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Kontak</th>
                  <th>Jabatan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php 
                foreach($pegawai as $u){ 
                ?>
                <tr>
                  <td><?php echo $u->nip?></td>
                  <td><?php echo $u->nama_pegawai?></td>
                  <td><?php if($u->jenis_kelamin=='L'){
                    echo 'Laki-laki';
                  }elseif ($u->jenis_kelamin=='P') {
                    echo 'Perempuan';
                  } ?>
                  </td>
                  <td><?php echo $u->telp?></td>
                  <td><?php if($u->jabatan==1){
                    echo 'Kepala Perpustakaan';
                  }elseif ($u->jabatan==2) {
                    echo 'Pustakawan';
                  } ?>
                  </td>
                  <td><center><button class="btn btn-success"><i class="fa fa-edit"></i> edit</button> 
                  <?php echo anchor('master/nonaktif/'.$u->id_pegawai,'<button class="btn btn-danger"><i class="fa fa-remove"></i> nonaktif</button>'); ?></center></td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Jenis Kelamin</th>
                  <th>Kontak</th>
                  <th>Jabatan</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
</div>
