<div class="col-sm-12">
	<div class="row">
   <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <a href="<?php echo base_url('master/siswa')?>"><button class="btn btn-success pull-left"><i class="fa fa-mail-reply"></i></button></a>
              <h3 class="box-title">Tambah data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'master/tambah_siswa'; ?>">
              <div class="box-body">
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3">NIS</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nis" required="" placeholder="0876643465387" onkeyup="angka(nilai, pesan)">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Nama</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nama_siswa" required="" placeholder="Nama Lengkap" onkeyup="huruf(nilai, pesan)">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jenis Kelamin</label>

                  <div class="col-sm-8">
                    <input type="radio"  name="jenis_kelamin"  value="L" required="" >Laki-laki
                    <input type="radio"  name="jenis_kelamin"  value="P" required="" >Perempuan
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Tempat Lahir</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="tempat_lahir"  placeholder="Kabupaten" onkeyup="huruf(nilai, pesan)">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">TGL Lahir</label>

                  <div class="col-sm-8">
                    <input type="date" class="form-control" name="tgl_lahir" onkeyup="">
                  </div>
                </div>
            	</div>
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3">Alamat</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="2" name="alamat" placeholder="Jalan, no rumah, Desa, Kecamatan, Kabupaten, Kode Pos " onkeyup=""></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Kelas</label>

                  <div class="col-sm-4">
                        <input type='text'
                              required="" 
                               placeholder='--kelas--'
                               class='flexdatalist form-control'
                               data-min-length='1'
                               data-selection-required='true'
                               list='tes'
                               name='id_kejur'>

                            <datalist id="tes">
                              <?php 
                              foreach($kelas as $u){ 
                              ?>
                                <option ><?php echo $u->id_kejur ?></option>
                              <?php }?>
                            </datalist>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Email</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="email" placeholder="contoh@gmail.com" >
                  </div>
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
    </div>
<script language="JavaScript">
            <!-- fungsi alphabet -->
            function huruf(nilai, pesan) {
                var alphaExp = /^[a-zA-Z]+$/;
                if(nilai.value.match(alphaExp)) {
                    return true;
                }
            }
            <!-- fungsi number -->
            function angka(nilai, pesan) {
                var numberExp = /^[0-9]+$/;
                if(nilai.value.match(numberExp)) {
                    return true;
                }
            }
            <!-- fungsi Email  -->
            function email(nilai, pesan) {
                var email = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
                if(nilai.value.match(email)) {
                    return true;
                }
            }
        </script>
