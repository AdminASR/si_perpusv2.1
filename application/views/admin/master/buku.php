<div class="box">
    <div class="box-header">
              <h3 class="box-title"><i class="fa fa-book"></i> Data Buku</h3>
              <a href="<?php echo base_url('master/form_tambah_buku')?>"><button class="btn btn-success pull-right"><i class="fa fa-plus"></i> Tambah Data</button></a>
    </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>Kode</th>
                  <th>Judul buku</th>
                  <th>Pengarang</th>
                  <th>Kategori</th>
                  <th>Lokasi</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($buku as $u){ ?>
                <tr>
                  <td>
                    <?php echo $u->kode_kategori?><?php $a; if($a=strlen($u->kode_buku)==1){echo"000";}elseif($a=strlen($u->kode_buku)==2){echo"00";}elseif($a=strlen($u->kode_buku)==3){echo"0";}elseif ($a=strlen($u->kode_buku)==4){echo "";}?><?php echo $u->kode_buku?>
                  </td>

                  <td><div class="pandek"><?php echo $u->judul?></div></td>
                  <td><div class="pandek"><?php echo $u->pengarang?></div></td>
                  <td><?php echo $u->kode_kategori?></td>
                  <td><?php echo $u->kode_rak?></td>
                  <td><center>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-info" onclick="tampildata('<?php echo $u->kode_buku ?>', '<?php echo $u->judul ?>', '<?php echo $u->kode_kategori ?>', '<?php echo $u->pengarang ?>', '<?php echo $u->penerbit ?>', '<?php echo $u->thn_terbit ?>', '<?php echo $u->kode_rak ?>', '<?php echo $u->jumlah ?>', '<?php echo $u->kondisi ?>', '<?php echo $u->foto ?>')"><i class="fa fa-file-text"></i> detail</button> 
                    <?php echo anchor('master/editb/'.$u->kode_buku,'<button type="button" class="btn btn-success"><i class="fa fa-edit"> Edit</i></button>'); ?>
                     <?php if ($u->foto == '-'){?>
                       <a  href="<?=base_url()?>index.php/master/hapusb/<?=$u->kode_buku?>" class="btn btn-danger"><i class="fa fa-trash"> Hapus</i></a>
                     <?php }else{?>
                       <a  href="<?=base_url()?>index.php/master/hapusb/<?=$u->kode_buku?>/<?=$u->foto?>" class="btn btn-danger"><i class="fa fa-trash"> Hapus</i></a>
                     <?php }?>
                     </center>
                  </td>
                </tr>
              <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Kode</th>
                  <th>Judul buku</th>
                  <th>Pengarang</th>
                  <th>Kategori</th>
                  <th>Lokasi</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
              </div>
              <div class="modal fade modal-info" id="modal-info" >
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Detail buku</h4>
                            </div>
                            <div class="modal-body box-header">
                    <table>
                    <tr >
                    <td rowspan="6"><img src="<?php echo base_url('assets/dist/img/buku.png')?>" id="" width="125px" height="125px" ></td>
                    </tr>
                    <tr>
                    <td> <div class="col-sm-1"></div><b>Kode Buku </b></td>
                    <td> : <span id="a"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>Judul</b></td>
                    <td> : <span id="b"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>Kategori</b></td>
                    <td> : <span id="c"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>Pengarang </b></td>
                    <td> : <span id="d"></span></td>
                    </tr>
                    <tr>
                    <td><div class="col-sm-1"></div><b>Penerbit </b></td>
                    <td> : <span id="e"></span></td>
                    </tr>
                    </table>
                    <br>
                    <table >
                    <tr>
                    <td><b>Tahun Terbit</b></td>
                    <td> : <span id="f"></span></td>
                    </tr>
                    <tr>
                    <td><b>Lokasi</b></td>
                    <td> : <span id="g"></span></td>
                    </tr>
                    <tr>
                    <td><b>Jumlah </b></td>
                    <td> : <span id="h"></span></td>
                    </tr>
                    <tr>
                    <td><b>Konidisi</b></td>
                    <td> : <span id="i"></span></td>
                    </tr>
                    </table>
                    
              </div>
              <div class="modal-footer" >
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<script>
    function tampildata(kode_buku, judul, kode_kategori, pengarang, penerbit, thn_terbit, kode_rak, jumlah, kondisi, foto){
       
      $('#a').html(kode_buku);
      $('#b').html(judul);
      $('#c').html(kode_kategori);
      $('#d').html(pengarang);
      $('#e').html(penerbit);
      $('#f').html(thn_terbit);
      $('#g').html(kode_rak);
      $('#h').html(jumlah);
      $('#i').html(kondisi);
      $('#j').attr('src', '../images/'+foto);
      $('#1').val(kode_buku);
      $('#2').val(judul);
      $('#3').val(kode_kategori);
      $('#4').val(pengarang);
      $('#5').val(penerbit);
      $('#6').val(thn_terbit);
      $('#7').val(kode_rak);
      $('#8').val(jumlah);
      $('#9').val(kondisi);
      }
      
</script>