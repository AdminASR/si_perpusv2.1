<div class="col-sm-12">
	<div class="row">
   <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah data</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'master/tambah_pegawai'; ?>">
              <div class="box-body">
                <div class="col-sm-6">
                <div class="form-group">
                  <label class="col-sm-3">NIP</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nip">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Nama</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nama_pegawai">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jenis Kelamin</label>

                  <div class="col-sm-8">
                    <input type="radio"  name="jenis_kelamin"  value="L" required="" >Laki-laki
                    <input type="radio"  name="jenis_kelamin"  value="P" required="" >Perempuan
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Telp</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="telp">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Alamat</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="alamat"></textarea>
                  </div>
                </div>
              </div>
                <div class="col-sm-6">
                  <br>
                  <br>
                  <br>
                <div class="form-group">
                  <label class="col-sm-3">User</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="user">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Pasword</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="password">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3">Jabatan</label>

                  <div class="col-sm-8">
                    <input type="radio"  name="jenis_kelamin"  value="1" required="" >Kepala Perpustakaan
                    <input type="radio"  name="jenis_kelamin"  value="2" required="" >Pustakawan
                  </div>
                </div>
              </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
    </div>