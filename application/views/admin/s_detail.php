<div class="box col-sm-10">
    <div class="box-header">
              <a href="<?php echo base_url('peminjam')?>"><button class="btn btn-success pull-left"><i class="fa fa-mail-reply"></i></button></a>
    </div>
    <div class="col-sm-1"></div><h3 class="box-title"><i class="fa fa-book"></i>Detail Peminjaman</h3>
    <?php 
    foreach ($detail_s as $s) {
    ?><h4><table>
    <tbody>
      <tr>
        <td><div class="col-sm-2"></div><b>Nama </b></td>
        <td><div class="col-sm-1"></div> : <?php echo $s->nama_siswa?></td>
      </tr>
      <tr>
        <td><div class="col-sm-2"></div><b>Kelas</b></td>
        <td><div class="col-sm-1"></div> : <?php echo $s->id_kejur?></td>
      </tr>
      <tr>
        <td><div class="col-sm-2"></div><b>Tanggal Pinjam</b></td>
        <td><div class="col-sm-1"></div> : <?php echo $s->tgl_pinjam?></td>
      </tr>
      <tr>
        <td><div class="col-sm-2"></div><b>Tanggal Kembali </b></td>
        <td><div class="col-sm-1"></div> : <?php echo $s->tgl_kembali?></td>
      </tr>
    </tbody>
  </table></h4>
<?php }?>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Kode Buku</th>
                  <th>Judul Buku</th>
                  <th>Jumlah Pinjam</th>
                  <th>Jumlah Kembali</th>
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  $n=1;
                  foreach ($detail_b as $db) {
                  ?>
                <tr>
                  <td><?php echo $n++?></td>
                  <td><?php echo $db->kode_buku?></td>
                  <td><?php echo $db->judul?></td>
                  <td><?php echo $db->jml?></td>
                  <td><?php echo $db->jml2?></td>
                  <td align="center">
                    <form method="post" action="<?php echo base_url('peminjam/kembali')?>">
                    <input type="number" name="jml_kembali" required="" id="id1" min="0" max="<?php echo $db->jml?>">
                    <input type="hidden" name="id_pinjam" value="<?php echo $db->id_pinjam?>">
                    <input type="hidden" name="jml" value="<?php echo $db->jml?>">
                    <input type="hidden" name="jml2" value="<?php echo $db->jml2?>">
                    <input type="hidden" name="kode_buku" value="<?php echo $db->kode_buku?>">
                    <button class="btn btn-primary btn-sm" ><i class="fa fa-edit"></i> Kembali</button>
                    </form>
                  </td>
                </tr>
                <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Kode Buku</th>
                  <th>Judul Buku</th>
                  <th>Jumlah Pinjam</th>
                  <th>Jumlah Kembali</th>
                  <th><center>Aksi</center></th>
                </tr>
                </tfoot>
              </table>
                    <form method="post" action="<?php echo base_url('peminjam/lengkap')?>">
                    <input type="hidden" name="id_pinjam" value="<?php echo $db->id_pinjam?>">
                    <button class="btn btn-success pull-right" ><i class="fa fa-check"></i> Lengkap</button>
                    </form>

            </div>
