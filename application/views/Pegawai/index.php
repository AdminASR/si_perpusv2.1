 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Tables
        <small>advanced tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
            </div>
            <!-- /.box-header -->



            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
               <tr>
                 

<?php if($this->session->userdata('jabatan')==='1'):?>
                 <a href="<?php echo base_url('index.php/pegawai/tambah') ?>" class="btn btn-success"><i class="fa fa-plus"></i>  Tambah Data</a>
<?php endif;  ?>
                <th>ID</th>
                <th>Username</th>
                <th>Password</th>
                <th>Nip</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Jabatan</th>
                <th>Alamat</th>
                <th>Telp</th>
                <th colspan="2">Aksi</th>
                

            </tr>
                </thead>
                <tbody>



                <?php
if( ! empty($pegawai)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
  foreach($pegawai as $data){ ?> 
              
              <tr>
                <td><?php echo $data->id_pegawai  ?></td>
                <td><?php echo $data->username  ?></td>
                <td><?php echo $data->password  ?></td>
                <td><?php echo $data->nip  ?></td>
                <td><?php echo $data->nama_pegawai ?></td>
                <td><?php echo $data->jenis_kelamin  ?></td>
                <?php if($data->jabatan == '1'){
                echo'<td>Admin</td>';
            }elseif($data->jabatan == '2'){
            echo'<td>pegawai</td>';
        }
        ?>
                
                <td><?php echo $data->alamat ?></td>
                <td><?php echo $data->telp  ?></td>
                <?php if($this->session->userdata('jabatan')==='1'):?>
                <td><a href="<?php echo base_url('index.php/pegawai/edit/'.$data->id_pegawai)?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a></td>
                 <td><a href="<?php echo base_url('index.php/pegawai/hapus/'.$data->id_pegawai)?>"  class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</a></td>
                 <?php endif;  ?>
            </tr>

              <?php  }
}
?>
                </tbody>
                <tfoot>
                <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Password</th>
                <th>Nip</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Jabatan</th>
                <th>Alamat</th>
                <th>Telp</th>
                <th colspan="2">Aksi</th>
                    

            </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->