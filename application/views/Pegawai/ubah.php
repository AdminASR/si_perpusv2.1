    <section class="content-header">
      <h1>
        Akun
        <small>Setting Akun</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
        <li><a href="#">Akun</a></li>
        <li class="active">Setting Akun</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
       
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
          
            <!-- /.box-header -->
            <!-- form start -->
            <?php foreach($pegawai as $data){ ?>
           <form class="form-horizontal" action="<?php echo base_url(). 'index.php/pegawai/update'; ?>" method="post">
              <div class="box-body">
                 <div class="form-group">
                  <label class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-4">
                  	<input type="hidden" name="id_pegawai" value="<?php echo $data->id_pegawai ?>">
                    <input type="text" class="form-control"  name="user" value="<?php echo $data->user ?>">
                  </div>
                   <label  class="col-sm-1 control-label">Password</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control"  name="password" placeholder="Masukkan password lama / buat password baru" >

                  </div>
                </div>
                
                 <div class="form-group">
                  <label  class="col-sm-2 control-label">NIP</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control"  name="nip" value="<?php echo $data->nip ?>">
                  </div>
                </div>
                 <div class="form-group">
                  <label  class="col-sm-2 control-label">Nama Pegawai</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control"  name="nama_pegawai" value="<?php echo $data->nama_pegawai ?>">
                  </div>
                </div>
                  <div class="form-group">
                  <label class="col-sm-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
                  <div class="col-sm-3">
           <label>
                    <input type="radio" <?php if ($data->jenis_kelamin=='L') {
                      echo "checked";
                    }?> name="jenis_kelamin" id="jenis_kelamin1"  required=""  value="L" >

          Laki-Laki
          </label>

        
                  </div>
                   <div class="col-sm-3">
          
          <label>
                    <input type="radio" <?php if ($data->jenis_kelamin=='P') {
                      echo "checked";
                    }?> name="jenis_kelamin" id="jenis_kelamin2"  required=""  value="P">
          Perempuan
          </label>
                  </div>
                </div>

                  <div class="form-group">
                  <label class="col-sm-2 control-label" for="jabatan">Jabatan</label>
                  <div class="col-sm-3">
           <label>
                    <input type="radio" <?php if ($data->jabatan=='1') {
                      echo "checked";
                    }?> name="jabatan" id="jabatan1"  required=""  value="1" >

          Kepala Perpustakaan
          </label>

        
                  </div>
                   <div class="col-sm-3">
          
          <label>
                    <input type="radio" <?php if ($data->jabatan=='2') {
                      echo "checked";
                    }?> name="jabatan" id="jabatan2"  required=""  value="2">
          Pustakawan
          </label>
                  </div>
                </div>
                 <div class="form-group">
                  <label  class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control"  name="alamat" value="<?php echo $data->alamat ?>">
                  </div>
                </div>
                 <div class="form-group">
                  <label  class="col-sm-2 control-label">No Telepon</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control"  name="telp" value="<?php echo $data->telp ?>">
                  </div>
                </div>
               
             
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <p class="pull-right">
                <a href="<?php echo base_url()?>" class="btn btn-danger ">Batal</a>
               <button type="submit" class="btn btn-info">Simpan</button></p>
               
               
               

              </div>
              <!-- /.box-footer -->
            </form>

            <?php } ?>
          </div>
          <!-- /.box -->
          
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>