<h1>Tambah Gambar</h1><hr>
<!-- Menampilkan Error jika validasi tidak valid -->
<div style="color: red;"><?php echo (isset($message))? $message : ""; ?></div>
<?php echo form_open("buku/tambah", array('enctype'=>'multipart/form-data')); ?>
  <table cellpadding="8">
    <tr>
      <td>kode_buku</td>
      <td><input type="text" name="input_kode_buku" value="<?php echo set_value('input_kode_buku'); ?>"></td>
    </tr>
      <tr>
      <td>judul</td>
      <td><input type="text" name="input_judul" value="<?php echo set_value('input_judul'); ?>"></td>
    </tr>
      <tr>
      <td>kategori</td>
      <td><input type="text" name="input_kategori" value="<?php echo set_value('input_kategori'); ?>"></td>
    </tr>
      <tr>
      <td>pengarang</td>
      <td><input type="text" name="input_pengarang" value="<?php echo set_value('input_pengarang'); ?>"></td>
    </tr>
      <tr>
      <td>penerbit</td>
      <td><input type="text" name="input_penerbit" value="<?php echo set_value('input_penerbit'); ?>"></td>
    </tr>
      <tr>
      <td>thn_terbit</td>
      <td><input type="text" name="input_thn_terbit" value="<?php echo set_value('input_thn_terbit'); ?>"></td>
    </tr>
      <tr>
      <td>lokasi</td>
      <td><input type="text" name="input_lokasi" value="<?php echo set_value('input_lokasi'); ?>"></td>
    </tr>
      <tr>
      <td>jumlah</td>
      <td><input type="text" name="input_jumlah" value="<?php echo set_value('input_jumlah'); ?>"></td>
    </tr>
      <tr>
      <td>deskripsi</td>
      <td><input type="text" name="input_deskripsi" value="<?php echo set_value('input_deskripsi'); ?>"></td>
    </tr>
    <tr>
      <td>Gambar</td>
      <td><input type="file" name="input_gambar"></td>
    </tr>
  </table>
    
  <hr>
  <input type="submit" name="submit" value="Simpan">
  <a href="<?php echo base_url(); ?>"><input type="button" value="Batal"></a>
<?php echo form_close(); ?>