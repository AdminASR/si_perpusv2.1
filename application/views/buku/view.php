   <div class="section section-grey">
    <div class="container">
      <div class="row">
        <div class="col-md-8" id="home-slick">
          <?php
              if( ! empty($event_1)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
                foreach($event_1 as $data){ ?> 
                 <div class="banner banner-2">
                  <img src="<?php echo base_url('images/event/'.$data->gambar)?>" alt="" height="535px">
                </div>
            <?php } }?> 
        </div>
        <p></p>
        <?php
        if( ! empty($event)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
          foreach($event as $data){ ?> 
              <div class="col-md-4 col-sm-6" id="home-slick">
                 <a class="banner banner-1" href="<?php echo base_url('event/detail/'.$data->id);?>" align="center">
                    <img src="<?php echo base_url('images/event/'.$data->gambar)?>" alt="">
                  <div class="banner-caption text-center">
                   <!--  <h2 class="white-color"><?php echo $data->judul  ?></h2> -->
                  </div>
                </a>
              </div>
          <p>.</p>
        <?php } }?> 
      </div>
    </div>
  </div>


  <div class="section">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- section-title -->
        <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">Buku Terpopuler</h2>
            <div class="pull-right">
              <div class="product-slick-dots-1 custom-dots"></div>
            </div>
          </div>

          <div class="row">
            <div id="product-slick-1" class="product-slick">
              <?php
              if( ! empty($buku_1)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
                foreach($buku_1 as $data){ ?> 
              <div class="product product-single">
                <div class="product-thumb">
                       <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata('<?php echo $data->kode_buku ?>','<?php echo $data->kode_rak ?>','<?php echo $data->kode_kategori ?>','<?php echo $data->judul ?>','<?php echo $data->pengarang ?>','<?php echo $data->penerbit ?>','<?php echo $data->thn_terbit ?>','<?php echo $data->jumlah ?>','<?php echo $data->deskripsi ?>','<?php echo $data->foto ?>')"><i class="fa fa-search-plus"></i> Detail</button>
                       <img src="<?php if($data->foto == '-'){
                          echo base_url('images/buku/default.png');
                        }else{
                          echo base_url('images/buku/'.$data->foto);
                        } ?>" alt="">
                </div>
                <div>
                  <table align="center">
                    <tr>
                      <td align="center"><p><b><h4><?php echo $data->judul ?></h4></b></p></td>
                    </tr>
                    <tr>
                      <td align="center"><b>(<?php echo $data->thn_terbit ?>)<?php echo $data->penerbit ?></b></td>
                    </tr>
                    <tr>
                      <td align="center"><b><?php echo $data->kode_kategori ?></b></td>
                    </tr>
                  </table><br>
                </div>
              </div>
            <?php  }}?>
            </div>
          </div>
        </div>


      <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">Rekomendasi untuk anda</h2>
            <div class="pull-right">
              <div class="product-slick-dots-2 custom-dots"></div>
            </div>
          </div>
          <div class="row">
            <div id="product-slick-2" class="product-slick">
              <?php
              if( ! empty($rkmnds_y)){ // Jika data pada database tidak sama dengan empty (alias ada datanya)
                foreach($rkmnds_y as $data){ ?> 
                  <div class="product product-single">
                    <div class="product-thumb">
                 <!--  <div class="product-label">
                    <span class="sale">terbaru</span>
                  </div> -->
                         <button type="button"  class="main-btn quick-view" data-toggle="modal" data-target="#myModal" onclick="tampildata('<?php echo $data->kode_buku ?>','<?php echo $data->kode_rak ?>','<?php echo $data->kode_kategori ?>','<?php echo $data->judul ?>','<?php echo $data->pengarang ?>','<?php echo $data->penerbit ?>','<?php echo $data->thn_terbit ?>','<?php echo $data->jumlah ?>','<?php echo $data->deskripsi ?>','<?php echo $data->foto ?>')"><i class="fa fa-search-plus"></i> Detail</button>
                           <img src="<?php if($data->foto == '-'){
                            echo base_url('images/buku/default.png');
                          }else{
                            echo base_url('images/buku/'.$data->foto);
                          } ?>" alt="">
                    </div>
                    <div>
                      <table align="center">
                        <tr>
                          <td align="center"><p><b><h4><?php echo $data->judul ?></h4></b></p></td>
                        </tr>
                        <tr>
                          <td align="center"><b>(<?php echo $data->thn_terbit ?>)<?php echo $data->penerbit ?></b></td>
                        </tr>
                        <tr>
                          <td align="center"><b><?php echo $data->kode_kategori ?></b></td>
                        </tr>
                      </table><br>
                   </div>
                  </div>
              <?php  }}?>
          </div>
        </div>
      </div>
    </div>



      <div class="row">
              <div class="col-md-6">
               <div class="section-title">
                  <h2 class="title">DENAH</h2> 
                </div>

          <div class="row">
            <div class="col-md-12">
              <div class="product product-single">
                <div class="product-thumb">
                  <a href="<?php echo base_url('assets_2/img/denah1.jpg')?>" class="main-btn quick-view"> <i class="fa fa-search-plus"></i> Detail</a>
                 <img src="<?php echo base_url('assets_2/img/denah2.jpg')?>" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="section-title">
            <h2 class="title">INFORMASI</h2>
            <h5 class="">(centang checkbox untuk lihat info)</h5>       
          </div>


          <div class="row">
              <div class="col-md-12">
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-1" class="pull-right" >
                <label for="shipping-1">Bagaimana Cara Meminjam Buku   ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Siswa Menyerahkan Kartu Anggota Perpustakaan</p>
                  <p><strong>2.</strong>&nbsp;Siswa Mencari buku yang akan dipinjam melalui Katalog</p>
                  <p><strong>3.</strong>&nbsp; Siswa Mengisi Formulir Peminjaman Buku, Tugas Akhir, atau Kuliah Kerja Praktek</p>
                  <p><strong>4.</strong>&nbsp;Petugas memberikan stempel tanda tanggal pengembalian buku yang akan di pinjam</p>
                  <p><strong>5.</strong>&nbsp;Petugas menyerahkan buku</p>
                  <p><strong>6.</strong>&nbsp;Maksimal lama peminjaman buku selama 3 hari</p>
                  <p><strong>7.</strong>&nbsp;Maksimal Perpanjangan Peminjaman buku sebanyak 3 kali</p>
                </div>
              </div>
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-2" class="pull-right" >
                <label for="shipping-2">Jam Berapa Perpustakaan Beroperasi  ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Senin (08.00–14.00)</p>
                  <p><strong>2.</strong>&nbsp;Selasa(08.00–14.00)</p>
                  <p><strong>3.</strong>&nbsp;Rabu  (08.00–14.00)</p>
                  <p><strong>4.</strong>&nbsp;Kamis (08.00–14.00)</p>
                  <p><strong>5.</strong>&nbsp;Jum'at(08.00–14.00)</p>
                </div>
              </div>
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-3" class="pull-right" >
                <label for="shipping-3">Bolehkah Pinjam Lebih Dari Satu(1) Buku  ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Maksimal peminjaman buku sebanyak 2 buku</p>
                </div>
              </div>
              <div class="input-checkbox">
                <input type="checkbox" name="shipping" id="shipping-4" class="pull-right" >
                <label for="shipping-4">Apa Sanksi Jika Telat Mengembalikan ?</label>
                <div class="caption">
                  <p><strong>1.</strong>&nbsp;Sangsi keterlambatan pengembalian buku pinjaman sebesar Rp.1.000 (Seribu Rupiah)</p>
                  <p><strong>2.</strong>&nbsp;Sangsi jika buku hilang atau rusak diganti dengan buku yang sama</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


   <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Detail Buku</h4>
        </div>
        <div class="modal-body">
         
        <div class="row">
          <div class="col-md-6">
            <h2 class="title">Foto</h2>
          <div class="row">
            <div class="col-md-12">
             <table >
              <tr>
              <td><img src="" id="val_foto" width="300px" height="400px" ></td>
             
               </table>
               <br>
              <br>
            </div>
          </div>
        </div>


        <div class="col-md-6">
            <h2 class="title">Keterangan</h2>
          <div class="row">
            <div class="col-md-12">
     <table >
        <tr>
            <td width="20%"><b>Judul</b></td>
            <td width="5%">:</td>
            <td><b><span id="val_judul"></span></b></td>
          </tr>
            <tr>
            <td width="20%"><b>Kode Rak</b></td>
            <td width="5%">:</td>
            <td><b><span id="val_kode_rak"></span></b></td>
          </tr>
         <tr>
            <td width="20%">Kode Buku</td>
            <td width="5%">:</td>
            <td><span id="val_kode_buku"></span></td>
          </tr>
         
           <tr>
            <td width="20%">Kategori</td>
            <td width="5%">:</td>
            <td><span id="val_kode_kategori"></span></td>
          </tr>
          <tr>
            <td>Pengarang</td>
            <td>:</td>
            <td><span id="val_pengarang"></span></td>
          </tr>
          <tr>
            <td>Penerbit</td>
            <td>:</td>
            <td><span id="val_penerbit"></span></td>
          </tr>
           <tr>
            <td>Tahun Terbit</td>
            <td>:</td>
            <td><span id="val_thn_terbit"></span></td>
          </tr>
          <tr>
            <td>Jumlah</td>
            <td>:</td>
            <td><span id="val_jumlah"></span></td>
          </tr>
          <tr>
            <td>Deskripsi</td>
            <td>:</td>
            <td><span id="val_deskripsi"></span></td>
          </tr>
      </table>
      </div>
     </div>
    </div>
   </div>
  </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
      </div>
    </div>
  </div>

