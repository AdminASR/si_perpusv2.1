
	<!-- /NAVIGATION -->

	<!-- BREADCRUMB -->
	<div id="breadcrumb">
		<div class="container">
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url('buku')?>">Home</a></li>
				<li class="active">Event</li>
			</ul>
		</div>
	</div>
	<!-- /BREADCRUMB -->

	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<?php foreach($event as $u){ ?>
   
        <div class="row">
				<!-- section-title -->
				<div class="col-md-6">
					
						<h2 class="title">Foto</h2>
						
						
					<div class="row">
						<div class="col-md-12">

					
						<div class="product-slick">
							
							<div>
								<div>
									<img src="<?php echo base_url('images/event/'.$u->gambar)?>" alt="" width="434px" height="308px">
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
				
				<br>
				<div class="col-md-6">
					
						<h2 class="title">Keterangan event</h2>
						
					
					<div class="row">
						<div class="col-md-12">


		 <table >
         
         	<tr>
         		<td width="20%">Judul</td>
         		<td width="5%">:</td>
         		
         		<td><?php echo $u->judul ?></td>
         	</tr>
         	<tr>
         		<td>Tanggal</td>
         		<td>:</td>
         		<td><?php echo $u->tanggal ?></td>
         	</tr>
         	<tr>
         		<td>Deskripsi</td>
         		<td>:</td>
         		<td><?php echo $u->deskripsi ?></td>
         	</tr>
         	
         </table>
       <?php } ?>
        		
						</div>
						
					</div>
				</div>
				<!-- /Product Slick -->
			</div>
			</div>


			
			
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->

	