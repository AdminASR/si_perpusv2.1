<?php 

class Login extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('M_Login');
		$this->load->model('M_Pegawai');

	}

	function index(){
		$this->load->view('login/index.php');
	}

	function aksi_login(){
    $user    = $this->input->post('user',TRUE);
    $password = md5($this->input->post('password',TRUE));
    $cek_login = $this->M_Login->cek_login($user,$password);

    if($cek_login->num_rows() > 0){
        $data  = $cek_login->row_array();
        $id_pegawai  = $data['id_pegawai'];
        $user  = $data['user'];
        $password = $data['password'];
       
        $jabatan = $data['jabatan'];
      

        
        $data_session = array(
        	'id_pegawai'     => $id_pegawai,
            'user'  => $user,
            'password'     => $password,
            'jabatan'     => $jabatan,
            'logged_in' => TRUE
        );

        $this->session->set_userdata($data_session);
        // access login for admin
        if($jabatan === '1'){
            redirect('home');

        // access login for staff
        }elseif($jabatan === '2'){
            redirect('home');

        // access login for author
        }else{
            redirect('pegawai/author');
        }
    }else{
        redirect('login');
    }
  }

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('index.php/login'));
	}
}