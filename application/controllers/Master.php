<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('AdminModel');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->model('M_Buku');
		$this->load->model('M_Pegawai');
		if($this->session->userdata('logged_in') !== TRUE){
			redirect('login');
		}
 
	}
	//<fungsi siswa>
	function siswa(){
		$data['siswa'] = $this->AdminModel->tampil1()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/siswa',$data);
		$this->load->view('admin/footer');
	}

	function form_tambah_siswa(){
		$data['kelas'] = $this->AdminModel->kelas()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/s_input',$data);
		$this->load->view('admin/footer');
	}

	function tambah_siswa(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
		$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
		$this->AdminModel->input_data($data,'siswa');
		redirect('master/siswa');
	}
	function hapuss($nis){
		$where = array('nis' => $nis);
		$this->AdminModel->hapus_data($where,'siswa');
		redirect('master/siswa');
	}
	
	function edits($nis){
		$where = array('nis' => $nis);
		$data['siswa'] = $this->AdminModel->edit_data($where,'siswa')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/s_edit',$data);
		$this->load->view('admin/footer');
	}
	function updates(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
 
	$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
 
	$where = array(
		'nis' => $nis
	);
 
	$this->AdminModel->update_data($where,$data,'siswa');
	redirect('master/siswa');
	}
	//</fungsi siswa>
	// <guru>
	function guru(){
		$data['guru'] = $this->AdminModel->guru()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/guru',$data);
		$this->load->view('admin/footer');
	}

	function form_tambah_guru(){
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/g_input');
		$this->load->view('admin/footer');
	}

	function tambah_guru(){
	$no_anggota = $this->input->post('no_anggota');
	$nama = $this->input->post('nama');
	$jabatan = $this->input->post('jabatan');
	$alamat = $this->input->post('alamat');
 
		$data = array(
		'no_anggota' => $no_anggota,
		'nama' => $nama,
		'jabatan' => $jabatan,
		'alamat' => $alamat
			);
		$this->AdminModel->input_data($data,'guru');
		redirect('master/guru');
	}
	function hapusg($no_anggota){
		$where = array('no_anggota' => $no_anggota);
		$this->AdminModel->hapus_data($where,'guru');
		redirect('master/guru');
	}
	
	function editg($no_anggota){
		$where = array('no_anggota' => $no_anggota);
		$data['guru'] = $this->AdminModel->edit_data($where,'guru')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/g_edit',$data);
		$this->load->view('admin/footer');
	}
	function updateg(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
 
	$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
 
	$where = array(
		'nis' => $nis
	);
 
	$this->AdminModel->update_data($where,$data,'siswa');
	redirect('master/siswa');
	}
	// </guru>
	//<fungsi buku>
	function buku(){
		$data['kategori'] = $this->AdminModel->kategori()->result();
		$dat['rak'] = $this->AdminModel->rak()->result();
		$data['buku'] = $this->AdminModel->tampil2()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/buku',$data,$dat);
		$this->load->view('admin/footer');
	}
	function form_tambah_buku(){
		$data['kategori'] = $this->AdminModel->kategori()->result();
		$data['rak'] = $this->AdminModel->rak()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/b_input',$data);
		$this->load->view('admin/footer');
	}

	function tambah_buku(){
    
	  $kode_buku   = $this->input->post('kode_buku');
      $kode_rak    = $this->input->post('kode_rak');
      $judul       = $this->input->post('judul');
      $kode_kategori    = $this->input->post('kode_kategori');
      $pengarang   = $this->input->post('pengarang');
      $penerbit    = $this->input->post('penerbit');
       $asal    = $this->input->post('asal');
      $thn_terbit  = $this->input->post('thn_terbit');
      $jumlah      = $this->input->post('jumlah');
      $kondisi      = $this->input->post('kondisi');
      $deskripsi   = $this->input->post('deskripsi');
      $status      = $this->input->post('status');

      // get foto
      $config['upload_path'] = './images/buku/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_size'] = '2048';  //2MB max
      // $config['max_width'] = '4480'; // pixel
      // $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $_FILES['foto_buku']['name'];

      $this->upload->initialize($config);

	    if (!empty($_FILES['foto_buku']['name'])) {
	            $this->upload->do_upload('foto_buku');
	            $foto = $this->upload->data();
	            $data = array(
	                          'kode_buku'    => $kode_buku,
	                          'kode_rak'     => $kode_rak,
	                          'judul'        => $judul,
	                          'kode_kategori'=> $kode_kategori,
	                          'pengarang'    => $pengarang,
	                          'penerbit'     => $penerbit,
	                          'asal'     => $asal,
	                          'thn_terbit'   => $thn_terbit,
	                          'jumlah'       => $jumlah,
	                          'kondisi'      => $kondisi,
	                          'deskripsi'    => $deskripsi,
	                          'status'       => $status,
	                          'foto'         => $foto['file_name']
	                        );
			 $this->M_Buku->tambah($data);
             redirect('master/buku');
	    }elseif (empty($_FILES['foto_buku']['name'])){
	      $this->upload->do_upload('foto_buku');
	            $foto = $this->upload->data();
	            $data = array(
	                          'kode_buku'    => $kode_buku,
	                          'kode_rak'     => $kode_rak,
	                          'judul'        => $judul,
	                          'kode_kategori'=> $kode_kategori,
	                          'pengarang'    => $pengarang,
	                          'penerbit'     => $penerbit,
	                          'asal'     => $asal,
	                          'thn_terbit'   => $thn_terbit,
	                          'jumlah'       => $jumlah,
	                          'kondisi'      => $kondisi,
	                          'deskripsi'    => $deskripsi,
	                          'status'       => $status,
	                          'foto'         => '-'
	                        );
			 $this->M_Buku->tambah($data);
             redirect('master/buku');
	    }
		
	}


    function hapusb($kode_buku,$foto){
    
      $path = './images/buku/';
      @unlink($path.$foto);

      $where = array('kode_buku' => $kode_buku , );
      $this->M_Buku->hapus($where);
      return redirect('master/buku');
  }
function updateb(){
	
	  $kode_buku   = $this->input->post('kode_buku');
      $kode_rak    = $this->input->post('kode_rak');
      $judul       = $this->input->post('judul');
      $kode_kategori    = $this->input->post('kode_kategori');
      $pengarang   = $this->input->post('pengarang');
      $penerbit    = $this->input->post('penerbit');
      $asal    = $this->input->post('asal');
      $thn_terbit  = $this->input->post('thn_terbit');
      $jumlah      = $this->input->post('jumlah');
      $kondisi      = $this->input->post('kondisi');
      $deskripsi   = $this->input->post('deskripsi');
      $status      = $this->input->post('status');

      $path 	   = './images/buku/';
      $where	   = array('kode_buku' => $kode_buku);

      // get foto
      $config['upload_path'] = './images/buku/';
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_size'] = '2048';  //2MB max
      // $config['max_width'] = '4480'; // pixel
      // $config['max_height'] = '4480'; // pixel
      $config['file_name'] = $_FILES['foto_buku']['name'];

      $this->upload->initialize($config);

	    if (empty($_FILES['foto_buku']['name'])) {
	          $this->upload->do_upload('foto_buku') ;
	            $foto = $this->upload->data();
	            $data = array(
	                          'kode_buku'    => $kode_buku,
	                          'kode_rak'     => $kode_rak,
	                          'judul'        => $judul,
	                          'kode_kategori'=> $kode_kategori,
	                          'pengarang'    => $pengarang,
	                          'penerbit'     => $penerbit,
	                          'asal'     	=> $asal,
	                          'thn_terbit'   => $thn_terbit,
	                          'jumlah'       => $jumlah,
	                          'kondisi'       => $kondisi,
	                          'deskripsi'    => $deskripsi,
	                          'status'       => $status,
	                          
	                          );

			 $this->M_Buku->ubah($data,$where);
             redirect('master/buku');
	        
	    }elseif(!empty($_FILES['foto_buku']['name'])) {
	    	  $this->upload->do_upload('foto_buku') ;
	            $foto = $this->upload->data();
	            $data = array(
	                          'kode_buku'    => $kode_buku,
	                          'kode_rak'     => $kode_rak,
	                          'judul'        => $judul,
	                          'kode_kategori'=> $kode_kategori,
	                          'pengarang'    => $pengarang,
	                          'penerbit'     => $penerbit,
	                          'asal'        => $asal,
	                          'thn_terbit'   => $thn_terbit,
	                          'jumlah'       => $jumlah,
	                          'kondisi'      => $kondisi,
	                          'deskripsi'    => $deskripsi,     
	       					  'foto'         => $foto['file_name']
	                          );

	         @unlink($path.$this->input->post('buku_lama'));

	         $this->M_Buku->ubah($data,$where);
             redirect('master/buku');
	    }

	}
	
	function editb($kode_buku){
		$data['kategori'] = $this->AdminModel->kategori()->result();
		$data['rak'] = $this->AdminModel->rak()->result();
		$where = array('kode_buku' => $kode_buku);
		$data['buku'] = $this->AdminModel->edit_data($where,'buku')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/b_edit',$data);
		$this->load->view('admin/footer');
	}
	
	//</fungsi buku>

	//<fungsi pegawai>
	function pegawai(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/pegawai',$data);
		$this->load->view('admin/footer');
	}
	function nonaktif($id_pegawai){
	$status = $this->input->post('status');
 
 
	$data = array(
		'status' => 2
	);
 
	$where = array(
		'id_pegawai' => $id_pegawai
	);
 
	$this->AdminModel->update_data($where,$data,'pegawai');
	redirect('master/pegawai');
	}
	function form_tambah_pegawai(){
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/p_input');
		$this->load->view('admin/footer');
	}

	function tambah_pegawai(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
		$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
		$this->AdminModel->input_data($data,'siswa');
		redirect('master/siswa');
	}
	
	function editp($nis){
		$where = array('nis' => $nis);
		$data['siswa'] = $this->AdminModel->edit_data($where,'siswa')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/master/s_edit',$data);
		$this->load->view('admin/footer');
	}
	function updatep(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
 
	$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
 
	$where = array(
		'nis' => $nis
	);
 
	$this->AdminModel->update_data($where,$data,'siswa');
	redirect('master/siswa');
	}
	//</fungsi pegawai>
 	
}
