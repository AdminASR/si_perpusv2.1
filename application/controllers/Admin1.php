<?php 
 
 
class Admin extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('AdminModel');
		$this->load->helper('url');
	}
 	//<home>
	function index(){
		$data['siswa'] = $this->AdminModel->tampil1()->result();
		$data['pengunjung'] = $this->AdminModel->pengunjung()->result();
		$data['jumlah1'] = $this->AdminModel->jumlah1()->num_rows();
		$data['jumlah2'] = $this->AdminModel->jumlah2()->num_rows();
		$data['jumlah3'] = $this->AdminModel->jumlah3();
		$data['jumlah4'] = $this->AdminModel->jumlah4();
		$this->load->view('admin/header');
		$this->load->view('admin/home',$data);
		$this->load->view('admin/footer'); 
	}

	function tambah_pengunjung(){
	$nis = $this->input->post('nis');
	$posisinis1=strpos($nis,"|")+1;
	$posisinis2=strpos($nis,"|");
	$ceknis=$posisinis2-$posisinis1;
	$nis=substr($nis,$posisinis1,$ceknis);
	$tanggal = $this->input->post('tanggal');
	$keterangan = $this->input->post('keterangan');
 
		$data = array(
		'nis' => $nis,
		'tanggal' =>$tanggal,
		'keterangan' => $keterangan
			);
		$this->AdminModel->input_data($data,'pengunjung');
		redirect('admin/index');
	}
	function edith($id_pengunjung){
		$where = array('id_pengunjung' => $id_pengunjung);
		$data['pengunjung'] = $this->AdminModel->edit_data($where,'pengunjung')->result();
		$this->load->view('admin/index',$data);
	}

	function hapush($id_pengunjung){
		$where = array('id_pengunjung' => $id_pengunjung);
		$this->AdminModel->hapus_data($where,'pengunjung');
		redirect('admin/index');
	}

	function updateh(){
	$keterangan = $this->input->post('keterangan');
 
 
	$data = array(
		'keterangan' => $keterangan
	);
 
	$where = array(
		'id_pengunjung' => $id_pengunjung
	);
 
	$this->AdminModel->update_data($where,$data,'pengunjung');
	redirect('admin/index');
	}
	//</home>
	
	function event(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/event',$data);
		$this->load->view('admin/footer');
	}
	function komentar(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/komentar',$data);
		$this->load->view('admin/footer');
	}
	function rekomendasi(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/rekomendasi',$data);
		$this->load->view('admin/footer');
	}
	function peminjam(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/peminjam',$data);
		$this->load->view('admin/footer');
	}
	function Lpeminjam(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/laporan/Lpeminjam',$data);
		$this->load->view('admin/footer');
	}

	//<fungsi siswa>
	function siswa(){
		$data['siswa'] = $this->AdminModel->tampil1()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/siswa',$data);
		$this->load->view('admin/footer');
	}

	function form_tambah_siswa(){
		$data['kelas'] = $this->AdminModel->kelas()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/s_input',$data);
		$this->load->view('admin/footer');
	}

	function tambah_siswa(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
		$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
		$this->AdminModel->input_data($data,'siswa');
		redirect('admin/siswa');
	}
	function hapuss($nis){
		$where = array('nis' => $nis);
		$this->AdminModel->hapus_data($where,'siswa');
		redirect('admin/siswa');
	}
	
	function edits($nis){
		$where = array('nis' => $nis);
		$data['siswa'] = $this->AdminModel->edit_data($where,'siswa')->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/s_edit',$data);
		$this->load->view('admin/footer');
	}
	function updates(){
	$nis = $this->input->post('nis');
	$nama_siswa = $this->input->post('nama_siswa');
	$jenis_kelamin = $this->input->post('jenis_kelamin');
	$tempat_lahir = $this->input->post('tempat_lahir');
	$tgl_lahir = $this->input->post('tgl_lahir');
	$alamat = $this->input->post('alamat');
	$id_kejur = $this->input->post('id_kejur');
	$email = $this->input->post('email');
 
 
	$data = array(
		'nis' => $nis,
		'nama_siswa' => $nama_siswa,
		'jenis_kelamin' => $jenis_kelamin,
		'tempat_lahir' => $tempat_lahir,
		'tgl_lahir' => $tgl_lahir,
		'alamat' => $alamat,
		'id_kejur' => $id_kejur,
		'email' => $email
			);
 
	$where = array(
		'nis' => $nis
	);
 
	$this->AdminModel->update_data($where,$data,'siswa');
	redirect('admin/siswa');
	}
	//</fungsi siswa>
	//<fungsi buku>
	function buku(){
		$data['kategori'] = $this->AdminModel->kategori()->result();
		$dat['rak'] = $this->AdminModel->rak()->result();
		$data['buku'] = $this->AdminModel->tampil2()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/buku',$data,$dat);
		$this->load->view('admin/footer');
	}
	function form_tambah_buku(){
		$data['kategori'] = $this->AdminModel->kategori()->result();
		$data['rak'] = $this->AdminModel->rak()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/b_input',$data);
		$this->load->view('admin/footer');
	}

	function tambah_buku(){
		$data = array();
		
		if($this->input->post('submit')){ // Jika user menekan tombol Submit (Simpan) pada form
			// lakukan upload file dengan memanggil function upload yang ada di GambarModel.php
			$upload = $this->SiswaModel->upload();
			
			if($upload['result'] == "success"){ // Jika proses upload sukses
				 // Panggil function save yang ada di GambarModel.php untuk menyimpan data ke database
				$this->SiswaModel->save($upload);
				
				redirect('admin/buku'); // Redirect kembali ke halaman awal / halaman view data
			}else{ // Jika proses upload gagal
				$data['message'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
		
		redirect('admin/buku');
	}

	function hapusb($kode_buku){
		$where = array('kode_buku' => $kode_buku);
		$this->AdminModel->hapus_data($where,'buku');
		redirect('admin/buku');
	}
	
	function editb($kode_buku){
		$data['kategori'] = $this->AdminModel->kategori()->result();
		$data['rak'] = $this->AdminModel->rak()->result();
		$where = array('kode_buku' => $kode_buku);
		$data['buku'] = $this->AdminModel->edit_data($where)->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/b_edit',$data);
		$this->load->view('admin/footer');
	}
	function updateb(){
	$kode_buku = $this->input->post('kode_buku');
	$judul = $this->input->post('judul');
	$id_kategori = $this->input->post('id_kategori');
	$pengarang = $this->input->post('pengarang');
	$penerbit = $this->input->post('penerbit');
	$thn_terbit = $this->input->post('thn_terbit');
	$id_rak = $this->input->post('id_rak');
	$jumlah = $this->input->post('jumlah');
	$deskripsi = $this->input->post('deskripsi');
	$foto = $this->input->post('foto');
	$status = $this->input->post('status');
 
 
	$data = array(
		'kode_buku' => $kode_buku,
		'judul' => $judul,
		'id_kategori' => $id_kategori,
		'pengarang' => $pengarang,
		'penerbit' => $penerbit,
		'thn_terbit' => $thn_terbit,
		'id_rak' => $id_rak,
		'jumlah' => $jumlah,
		'deskripsi' => $deskripsi,
		'foto' => $foto,
		'status' => $status
	);
 
	$where = array(
		'kode_buku' => $kode_buku
	);
 
	$this->AdminModel->update_data($where,$data,'buku');
	redirect('admin/buku');
	}
	//</fungsi buku>

	//<fungsi pegawai>
	function pegawai(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/master/pegawai',$data);
		$this->load->view('admin/footer');
	}
	function nonaktif($id_pegawai){
	$status = $this->input->post('status');
 
 
	$data = array(
		'status' => 2
	);
 
	$where = array(
		'id_pegawai' => $id_pegawai
	);
 
	$this->AdminModel->update_data($where,$data,'pegawai');
	redirect('admin/pegawai');
	}

	//</fungsi pegawai>
 	//<fungsi pengunjung>
	function Lpengunjung(){
		$data['pegawai'] = $this->AdminModel->tampil3()->result();
		$this->load->view('admin/header');
		$this->load->view('admin/laporan/Lpengunjung',$data);
		$this->load->view('admin/footer');
	}

}