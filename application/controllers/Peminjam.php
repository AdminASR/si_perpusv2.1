<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjam extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('AdminModel');
		$this->load->helper('url');
		$this->load->model('M_Pegawai');
		if($this->session->userdata('logged_in') !== TRUE){
			redirect('login');
		}
	}
		
	function index(){
		$data['buku'] = $this->AdminModel->tampil2()->result();
		$data['guru'] = $this->AdminModel->guru()->result();
		$data['siswa'] = $this->AdminModel->tampil1()->result();
		$data['peminjam'] = $this->AdminModel->peminjam1()->result();
		$data['gpeminjam'] = $this->AdminModel->gpeminjam1()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/peminjam',$data);
		$this->load->view('admin/footer');
	}
	//siswa
	function s_detail($id_pinjam){
		$data['detail_s'] = $this->AdminModel->s_detail_2($id_pinjam)->result();
		$data['detail_b'] = $this->AdminModel->s_detail($id_pinjam)->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/s_detail',$data);
		$this->load->view('admin/footer');
	}
	function kembali(){
		$id_pinjam = $this->input->post('id_pinjam');
		$jml_pinjam = $this->input->post('jml');
		$jml_ka = $this->input->post('jml2');
		$kode_buku = $this->input->post('kode_buku');
		$jml_kembali = $this->input->post('jml_kembali');
		$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
		$where1=$id_pinjam;
		$where2=$kode_buku;
		$data2=$jml_ka+$jml_kembali;
				$jml1=0;
				foreach ($cek_stok as $row1){
			$jml1=$row1->jumlah;
			if ($data2>$jml_pinjam) {

				redirect('peminjam/s_detail/'.$id_pinjam);
			}
				$upstok=$jml1+$jml_kembali;
				

				$data = array(
					'jumlah' => $upstok
				);
	 
				$where = array(
					'kode_buku' => $kode_buku
				);
				$this->AdminModel->update_data($where,$data,'buku');
				$this->AdminModel->update_data2($where1,$where2,$data2,'detail_peminjaman');
		}
		redirect('peminjam/s_detail/'.$id_pinjam);
	}
	function lengkap(){
		$id_pinjam = $this->input->post('id_pinjam');
		$status=0;
		$data = array(
					'status' => $status
				);
	 
		$where = array(
					'id_pinjam' => $id_pinjam
				);
				$this->AdminModel->update_data($where,$data,'peminjam');
			redirect('pengembalian');
	}
	function g_detail($id_gpinjam){
		$data['detail_g'] = $this->AdminModel->g_detail_2($id_gpinjam)->result();
		$data['detail_b'] = $this->AdminModel->g_detail($id_gpinjam)->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/g_detail',$data);
		$this->load->view('admin/footer');

	}

	function gkembali(){
		$id_gpinjam = $this->input->post('id_gpinjam');
		$jml_pinjam = $this->input->post('jml');
		$jml_ka = $this->input->post('jml2');
		$kode_buku = $this->input->post('kode_buku');
		$jml_kembali = $this->input->post('jml_kembali');
		$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
		$where1=$id_gpinjam;
		$where2=$kode_buku;
		$data2=$jml_ka+$jml_kembali;
				$jml1=0;
				foreach ($cek_stok as $row1){
			$jml1=$row1->jumlah;
			if ($data2>$jml_pinjam) {

				redirect('peminjam/g_detail/'.$id_gpinjam);
			}
				$upstok=$jml1+$jml_kembali;
				

				$data = array(
					'jumlah' => $upstok
				);
	 
				$where = array(
					'kode_buku' => $kode_buku
				);
				$this->AdminModel->update_data($where,$data,'buku');
				$this->AdminModel->update_data3($where1,$where2,$data2,'detail_gpeminjaman');
		}
	redirect('peminjam/g_detail/'.$id_gpinjam);
	}

	function glengkap(){
		$id_gpinjam = $this->input->post('id_gpinjam');
		$status=0;
		$data = array(
					'status' => $status
				);
	 
		$where = array(
					'id_gpinjam' => $id_gpinjam
				);
				$this->AdminModel->update_data($where,$data,'gpeminjam');
			redirect('pengembalian');
	}
	function tambah_peminjam(){
	$nis = $this->input->post('nis');
	$posisinis1=strpos($nis,"[")+1;
	$posisinis2=strpos($nis,"]");
	$ceknis=$posisinis2-$posisinis1;
	$nis=substr($nis,$posisinis1,$ceknis);
	$keterangan = $this->input->post('keterangan');
	$tgl_pinjam = $this->input->post('tgl_pinjam');
	$tgl_kembali = $this->input->post('tgl_kembali');
	$status = $this->input->post('status');
	$cek=$this->db->query('select max(id_pinjam) as id_pinjam from peminjam')->result();
	$kode=0;
	foreach ($cek as $row){
		$kode=$row->id_pinjam+1;
	}
		$data = array(
		'id_pinjam'=>$kode,
		'nis' => $nis,
		'tgl_pinjam' => $tgl_pinjam,
		'tgl_kembali' => $tgl_kembali,
		'keterangan' => $keterangan,
		'status' => $status
			);
		$this->AdminModel->input_data($data,'peminjam');
		
		   $i=0;
		   $n=$this->input->post('idf');
		while($i<$n){
			$kode_buku = $_POST['kode_buku'][$i];
			$jml = $_POST['jml'][$i];
			$a=strpos($kode_buku,"[")+1;
			$b=strpos($kode_buku,"]");
			$c=$b-$a;
			$kode_buku=substr($kode_buku,$a,$c);
			$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
			$jml1=0;
			foreach ($cek_stok as $row1){
				$jml1=$row1->jumlah;
				$upstok=$jml1-$jml;
			}
			$data_b=array(
				'jumlah'=>$upstok
			);
			$where=array(
				'kode_buku'=>$kode_buku
			);
			$this->AdminModel->update_data_b($where,$data_b,'buku');
			 $data = array(
			'id_pinjam' => $kode,
			'kode_buku' => $kode_buku,
			'jml' => $jml
			);
			$this->AdminModel->input_data($data,'detail_peminjaman');
		   $i++;
		}
		
		redirect('peminjam');
	}

	//guru
	function tambah_gpeminjam(){
	$no_anggota = $this->input->post('no_anggota');
	$posisinis1=strpos($no_anggota,"[")+1;
	$posisinis2=strpos($no_anggota,"]");
	$ceknis=$posisinis2-$posisinis1;
	$no_anggota=substr($no_anggota,$posisinis1,$ceknis);
	$keterangan = $this->input->post('keterangan');
	$tgl_pinjam = $this->input->post('tgl_pinjam');
	$tgl_kembali = $this->input->post('tgl_kembali');
	$status = $this->input->post('status');
	$cek=$this->db->query('select max(id_gpinjam) as id_gpinjam from gpeminjam')->result();
	$kode=0;
	foreach ($cek as $row){
		$kode=$row->id_gpinjam+1;
	}
		$data = array(
		'id_gpinjam'=>$kode,
		'no_anggota' => $no_anggota,
		'tgl_pinjam' => $tgl_pinjam,
		'tgl_kembali' => $tgl_kembali,
		'keterangan' => $keterangan,
		'status' => $status
			);
		$this->AdminModel->input_data($data,'gpeminjam');
		
		   $i=0;
		   $n=$this->input->post('idfg');
		   while($i<$n){
			$kode_buku = $_POST['kode_buku'][$i];
			$jml = $_POST['jml'][$i];echo $jml;
			$a=strpos($kode_buku,"[")+1;
			$b=strpos($kode_buku,"]");
			$c=$b-$a;
			$kode_buku=substr($kode_buku,$a,$c);
			$cek_stok=$this->db->query("select jumlah from buku where kode_buku='$kode_buku'")->result();
			$jml1=0;
			foreach ($cek_stok as $row1){
		$jml1=$row1->jumlah;
		$upstok=$jml1-$jml;
	}
			$data_b=array(
				'jumlah'=>$upstok
			);
			$where=array(
				'kode_buku'=>$kode_buku
			);
			$this->AdminModel->update_data_b($where,$data_b,'buku');
//cek stok buku berdasarkan kode, tampiljkan hasil stok, kurangi stok dengan yang dipinjam.

			//update tbl buku, stok
			

			 $data = array(
			'id_gpinjam' => $kode,
			'kode_buku' => $kode_buku,
			'jml' => $jml
				);
			$this->AdminModel->input_data($data,'detail_gpeminjaman');
		   $i++;
		   }
		
		redirect('peminjam');
	}
}
