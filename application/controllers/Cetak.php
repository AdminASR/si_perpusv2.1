<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {

  function __construct(){
    parent::__construct();  
    $this->load->library('pdf');  
    $this->load->model('AdminModel');
    $this->load->helper('url');
    $this->load->model('M_Pegawai');
    if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
    }
  }
  function pengunjung_harian(){
    $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',12);
        // mencetak string 
        $pdf->Image("assets/dist/img/jatim.png",6,10,27);
        $pdf->Image("assets/dist/img/smk.png",180,10,18);
        $pdf->Cell(190,7,'PEMERINTAH PROVINSI JAWA TIMUR',0,1,'C');
        $pdf->Cell(190,7,' DINAS PENDIDIKAN',0,1,'C');
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUAN NEGERI 6 JEMBER',0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,7,'Jalan PB. Sudirman 114 Tanggul Telp. / Fax. (0336) 441347 Jember 68155',0,1,'C');
        $pdf->Cell(190,7,'E-mail : smkn6.jember@yahoo.com ; Website : smkn6jember.sch.id',0,1,'C');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7,6,'NO',1,0);
        $pdf->Cell(20,6,'Waktu',1,0);
        $pdf->Cell(73,6,'NAMA SISWA',1,0);
        $pdf->Cell(25,6,'KELAS',1,0);
        $pdf->Cell(65,6,'KETERANGAN',1,1);



        $pdf->SetFont('Arial','',10);
        $where = $this->input->post('tanggal');
        $data['tes']=$where;
        $laporan = $this->AdminModel->Lpengunjung($where,'pengunjung')->result();
        $n=1;
        foreach ($laporan as $row){
            $pdf->Cell(7,6,$n++,1,0);
            $pdf->Cell(20,6,$cek=substr($row->tanggal,11),1,0);
            $pdf->Cell(73,6,$row->nama_siswa,1,0);
            $pdf->Cell(25,6,$row->id_kejur,1,0);
            $pdf->Cell(65,6,$row->keterangan,1,1);
        }
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(190,3,'Form   : F.05.SAR.6.3 PUS.19',0,1);
        $pdf->Cell(190,3,'Revisi : '.date("Y-m-d"),0,1);
        //$pdf->Image("assets/dist/img/download.jpg",178,194,13);
        $pdf->SetFont('Arial','I',6);
        $pdf->Cell(190,3,'Cert. No. 46814/A/0001/UK/En',0,1,'R');

        $pdf->Output();
  }
  function pengunjung_bulanan(){
    $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',12);
        // mencetak string 
        $pdf->Image("assets/dist/img/jatim.png",6,10,27);
        $pdf->Image("assets/dist/img/smk.png",180,10,18);
        $pdf->Cell(190,7,'PEMERINTAH PROVINSI JAWA TIMUR',0,1,'C');
        $pdf->Cell(190,7,' DINAS PENDIDIKAN',0,1,'C');
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(190,7,'SEKOLAH MENENGAH KEJURUAN NEGERI 6 JEMBER',0,1,'C');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(190,7,'Jalan PB. Sudirman 114 Tanggul Telp. / Fax. (0336) 441347 Jember 68155',0,1,'C');
        $pdf->Cell(190,7,'E-mail : smkn6.jember@yahoo.com ; Website : smkn6jember.sch.id',0,1,'C');

        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7,6,'NO',1,0);
        $pdf->Cell(20,6,'Waktu',1,0);
        $pdf->Cell(60,6,'NAMA SISWA',1,0);
        $pdf->Cell(25,6,'NOMOR ANGGOTA',1,0);
        $pdf->Cell(25,6,'KELAS',1,0);
        $pdf->Cell(65,6,'KETERANGAN',1,0);
        $pdf->Cell(65,6,'JUMLAH PENGUNJUNG',1,1);



        $pdf->SetFont('Arial','',10);
          $bulan = $this->input->post('bulan');
          $posisibln1=strpos($bulan,"(")+1;
          $posisibln2=strpos($bulan,")");
          $cekbln=$posisibln2-$posisibln1;
          $bulan=substr($bulan,$posisibln1,$cekbln);
          $tahun= $this->input->post('tahun');
          $jurusan= $this->input->post('jurusan');
          $where=1;
          $data['lbl1']=$bulan;
          $data['lbl2']=$tahun;
          $laporan = $this->AdminModel->pengunjung_bulanan2($jurusan,$bulan,$tahun,'pengunjung')->result();
        $n=1;
        foreach ($laporan as $row){
            $pdf->Cell(7,6,$n++,1,0);
            $pdf->Cell(20,6,$cek=substr($row->tanggal,11),1,0);
            $pdf->Cell(60,6,$row->nama_siswa,1,0);
            $pdf->Cell(25,6,$row->nis,1,0);
            $pdf->Cell(25,6,$row->id_kejur,1,0);
            $pdf->Cell(65,6,$row->keterangan,1,1);
        }
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(190,3,'Form   : F.05.SAR.6.3 PUS.19',0,1);
        $pdf->Cell(190,3,'Revisi : '.date("Y-m-d"),0,1);
        //$pdf->Image("assets/dist/img/download.jpg",178,194,13);
        $pdf->SetFont('Arial','I',6);
        $pdf->Cell(190,3,'Cert. No. 46814/A/0001/UK/En',0,1,'R');

        $pdf->Output();
  }
}