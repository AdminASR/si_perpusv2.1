<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('AdminModel');
		$this->load->helper('url');
		$this->load->model('M_Pegawai');
		if($this->session->userdata('logged_in') !== TRUE){
			redirect('login');
		}
	}
//<pengunjung>

	

	function hapuslg($id_gpengunjung){
		$where = array('id_gpengunjung' => $id_gpengunjung);
		$this->AdminModel->hapus_data($where,'gpengunjung');
		redirect('laporan/Lpengunjung');
	}
	function hapusls($id_pengunjung){
		$where = array('id_pengunjung' => $id_pengunjung);
		$this->AdminModel->hapus_data($where,'pengunjung');
		redirect('laporan/Lpengunjung');
	}
	function updatels(){
		$keterangan = $this->input->post('keterangan');
		$id_pengunjung = $this->input->post('id_pengunjung');
	 
	 
		$data = array(
			'keterangan' => $keterangan
		);
	 
		$where = array(
			'id_pengunjung' => $id_pengunjung
		);
	 
		$this->AdminModel->update_data($where,$data,'pengunjung');
		redirect('laporan/Lpengunjung');
	}

	function updatelg(){
		$keterangan = $this->input->post('keterangan');
		$id_gpengunjung = $this->input->post('id_gpengunjung');
	 
	 
		$data = array(
			'keterangan' => $keterangan
		);
	 
		$where = array(
			'id_gpengunjung' => $id_gpengunjung
		);
	 
		$this->AdminModel->update_data($where,$data,'gpengunjung');
			redirect('laporan/Lpengunjung');
	}

	function Lpengunjung(){
		$where = $this->input->post('tanggal');
		$where2 = $this->input->post('tanggal2');
		$data['tes']=$where;
		$data['tes2']=$where2;
		$data['gpengunjung'] = $this->AdminModel->Lgpengunjung($where,$where2,'gpengunjung')->result();
		$data['pengunjung'] = $this->AdminModel->Lpengunjung($where,$where2,'pengunjung')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/Lpengunjung',$data);
		$this->load->view('admin/footer');
	}

	function pengunjung_harian(){
		$where = $this->input->post('tanggal');
		if ($where==NULL) {
			$where=date("Y-m-d");
		}
		$data['tes']=$where;
		$data['pengunjung'] = $this->AdminModel->Lpengunjung($where,'pengunjung')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/hr_pengunjung',$data);
		$this->load->view('admin/footer');
	}
	function pengunjung_bulanan(){
		$bulan = $this->input->post('bulan');
		$posisibln1=strpos($bulan,"(")+1;
		$posisibln2=strpos($bulan,")");
		$cekbln=$posisibln2-$posisibln1;
		$bulan=substr($bulan,$posisibln1,$cekbln);
		$tahun= $this->input->post('tahun');
		$data['lbl1']=$bulan;
		$data['lbl2']=$tahun;
		$data['pengunjung'] = $this->AdminModel->pengunjung_bulanan($bulan,$tahun,'pengunjung')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/bln_pengunjung');
		$this->load->view('admin/footer');
	}

//</pengunjung>
	function Lpeminjaman(){
		$data['peminjam'] = $this->AdminModel->lpeminjam()->result();
		$data['peminjam1'] = $this->AdminModel->lpeminjam1()->result();
		$data['peminjam2'] = $this->AdminModel->lpeminjam2()->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/Lpeminjam',$data);
		$this->load->view('admin/footer');
	}
	function peminjam_harian(){
		$where = $this->input->post('tanggal');
		if ($where==NULL) {
			$where=date("Y-m-d");
		}
		$data['tes']=$where;
		$data['peminjam'] = $this->AdminModel->Lpeminjam($where,'peminjam')->result();
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/hr_peminjam');
		$this->load->view('admin/footer');
	}
	function html(){
		$bulan = $this->input->post('bulan');
		$posisibln1=strpos($bulan,"(")+1;
		$posisibln2=strpos($bulan,")");
		$cekbln=$posisibln2-$posisibln1;
		$bulan=substr($bulan,$posisibln1,$cekbln);
		if ($bulan==NULL) {
			$bulan=date("m");
		}
		$tahun= $this->input->post('tahun');
		if ($tahun==NULL) {
			$tahun=date("Y");
		}
		$tgl=1;
		while ($tgl <= 31) {
			$n=strlen($tgl);
			if ($n==1) {
				$a=0;
			}elseif ($n==2) {
				$a=NULL;
			}
		$data['peminjam0$a$tgl'] = $this->AdminModel->peminjam_bulanan0($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam1$a$tgl'] = $this->AdminModel->peminjam_bulanan1($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam2$a$tgl'] = $this->AdminModel->peminjam_bulanan2($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam3$a$tgl'] = $this->AdminModel->peminjam_bulanan3($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam4$a$tgl'] = $this->AdminModel->peminjam_bulanan4($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam5$a$tgl'] = $this->AdminModel->peminjam_bulanan5($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam6$a$tgl'] = $this->AdminModel->peminjam_bulanan6($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam7$a$tgl'] = $this->AdminModel->peminjam_bulanan7($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam8$a$tgl'] = $this->AdminModel->peminjam_bulanan8($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
		$data['peminjam9$a$tgl'] = $this->AdminModel->peminjam_bulanan9($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();echo $data['peminjam0$a$tgl'];
		$data['peminjam10$a$tgl'] = $this->AdminModel->peminjam_bulanan10($a,$tgl,$bulan,$tahun,'peminjam')->num_rows();
			$tgl++;
		}
		$data['lbl1']=$bulan;
		$data['lbl2']=$tahun;
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/bln_peminjam',$data);
		$this->load->view('admin/footer');
	}

	function peminjam_bulanan(){
		$data['notif2'] = $this->AdminModel->notif()->num_rows();
		$data['notif'] = $this->AdminModel->lpeminjam1()->result();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/laporan/html',$data);
		$this->load->view('admin/footer');
	}
}
