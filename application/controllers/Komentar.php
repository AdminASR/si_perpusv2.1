<?php 
 
 
class Komentar extends CI_Controller{
 
	function __construct(){
		parent::__construct();	
        $this->load->model('AdminModel');	
		$this->load->model('M_Komentar');
        $this->load->model('M_Pegawai');
         if($this->session->userdata('logged_in') !== TRUE){
      redirect('login');
  }
	}
 	//<home>
 function index(){
    $data['komentar'] = $this->M_Komentar->view();
    $data['sb'] = $this->M_Komentar->sangat_bagus()->num_rows();
    $data['b'] = $this->M_Komentar->bagus()->num_rows();
    $data['c'] = $this->M_Komentar->cukup()->num_rows();
    $data['j'] = $this->M_Komentar->buruk()->num_rows();
    $data['sj'] = $this->M_Komentar->sangat_buruk()->num_rows();
    $data['notif2'] = $this->AdminModel->notif()->num_rows();
	$data['notif'] = $this->AdminModel->lpeminjam1()->result();
	$this->load->view('admin/header',$data);
    $this->load->view('admin/komentar', $data);
    $this->load->view('admin/footer');
   
	}

	 function tambah(){
        $this->load->view('u_header.php');
        $this->load->view('buku/kontak');
        $this->load->view('u_footer.php');
    }
    
    function hapus($id){
		$where = array ('id'=>$id);
		$this->M_Komentar->hapus_data($where,'komentar');
		redirect('komentar/index');
	}
}
?>