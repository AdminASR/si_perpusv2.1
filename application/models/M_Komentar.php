<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_Komentar extends CI_Model {
  // Fungsi untuk menampilkan semua data buku
  public function view(){
    return $this->db->get('komentar')->result();
  }
  public function sangat_bagus(){
    return $this->db->query("select * from komentar where rating='5'");
  }
  public function bagus(){
    return $this->db->query("select * from komentar where rating='4'");
  }
  public function cukup(){
    return $this->db->query("select * from komentar where rating='3'");
  }
  public function buruk(){
    return $this->db->query("select * from komentar where rating='2'");
  }
  public function sangat_buruk(){
    return $this->db->query("select * from komentar where rating='1'");
  }

  function input_data($data,$table){
    $this->db->insert($table,$data);
  }
  function edit_data($where,$table){
    return $this->db->get_where($table,$where);
  }
  function update_data($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  }
  function hapus_data($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  } 

  function lihat($table,$where){    
    return $this->db->get_where($table,$where);
  } 
  
  // // Fungsi untuk melakukan proses upload file
  // public function upload(){
  //   $config['upload_path'] = './images/';
  //   $config['allowed_types'] = 'jpg|png|jpeg';
  //   $config['max_size']  = '2048';
  //   $config['remove_space'] = TRUE;
  //   $config['max_width']='1049';
  //   $config['max_height']='1600';
  
  //   $this->load->library('upload', $config); // Load konfigurasi uploadnya
  //   if($this->upload->do_upload('input_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
  //     // Jika berhasil :
  //     $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
  //     return $return;
  //   }else{
  //     // Jika gagal :
  //     $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
  //     return $return;
  //   }
  // }
  
  // // Fungsi untuk menyimpan data ke database
  // public function save($upload){
  //   $data = array(
  //     'kode_buku'=>$this->input->post('input_kode_buku'),
  //     'judul'=>$this->input->post('input_judul'),
  //     'kategori'=>$this->input->post('input_kategori'),
  //     'pengarang'=>$this->input->post('input_pengarang'),
  //     'penerbit'=>$this->input->post('input_penerbit'),
  //     'thn_terbit'=>$this->input->post('input_thn_terbit'),
  //     'lokasi'=>$this->input->post('input_lokasi'),
  //     'jumlah'=>$this->input->post('input_jumlah'),
  //     'deskripsi'=>$this->input->post('input_deskripsi'),
  //     'foto' => $upload['file']['file_name']
  //   );
    
  //   $this->db->insert('buku', $data);
  // }
}