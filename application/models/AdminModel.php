<?php

class AdminModel extends CI_Model{


	function notif(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_kembali='$date'limit 5");
	}

	function kelas(){
		return $this->db->query('select * from kelas');
	}
	function jurusan(){
		return $this->db->query('select * from jurusan');
	}
	function keterangan(){
		return $this->db->query('select * from keterangan');
	}
	function kategori(){
		return $this->db->query('select * from kategori');
	}
	function rak(){
		return $this->db->query('select * from rak');
	}
	function guru(){
		return $this->db->query('select * from guru');
	}

	function tes(){
		return $this->db->query('select * from pengunjung inner join siswa on pengunjung.nis=siswa.nis');
	}

	function peminjam(){
		return $this->db->query('select * from peminjam p join siswa s on p.nis=s.nis where p.status=1');
	}

	function gpeminjam(){
		return $this->db->query('select * from gpeminjam p inner join guru g on p.no_anggota=g.no_anggota where p.status=1');
	}

	function peminjam1(){
		return $this->db->query('select * from peminjam p join siswa s on p.nis=s.nis where p.status=1');
	}

	function gpeminjam1(){
		return $this->db->query('select * from gpeminjam p inner join guru g on p.no_anggota=g.no_anggota where p.status=1');
	}

	function s_detail($where){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where d.id_pinjam=$where");
	}
	function s_detail_2($where){
		return $this->db->query("select * from peminjam p join siswa s on p.nis=s.nis where id_pinjam=$where");
	}
	function g_detail($where){
		return $this->db->query("select * from guru g inner join gpeminjam p on g.no_anggota=p.no_anggota inner join detail_gpeminjaman d on p.id_gpinjam=d.id_gpinjam inner join buku b on d.kode_buku=b.kode_buku where d.id_gpinjam=$where");
	}
	function g_detail_2($where){
		return $this->db->query("select * from gpeminjam p join guru g on p.no_anggota=g.no_anggota where id_gpinjam=$where");
	}

	function pengunjung(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from pengunjung inner join siswa on pengunjung.nis=siswa.nis where tanggal like '$date%'");
	}
	
	function gpengunjung(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from gpengunjung inner join guru on gpengunjung.no_anggota=guru.no_anggota where tanggal like '$date%'");
	}
	function tampil5(){
		return $this->db->query('select * from event ');
	}
	function tampil4(){
		return $this->db->query('select * from guru ');
	}
	function tampil1(){
		return $this->db->query('select * from siswa ');
	}
	function tampil2(){
		return $this->db->query('select * from buku');
	}
	function tampil3(){
		return $this->db->query('select * from pegawai where not status=2');
	}
	
	function jumlah1(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from pengunjung inner join siswa on pengunjung.nis=siswa.nis where tanggal like '$date%'");;
	}
	function jumlah5(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from gpengunjung inner join guru on gpengunjung.no_anggota=guru.no_anggota where tanggal like '$date%'");;
	}
	function jumlah2(){
		return $this->db->query('select * from peminjam where status=1');
	}
	function jumlah3(){
		$this->db->select_sum('jumlah');
		$query=$this->db->get('buku');
		if ($query->num_rows()>0) {
			return $query->row()->jumlah;
		}else{
			return 0;
		}
	}

	function jumlah41(){
		$this->db->select_sum('jml');
		$query=$this->db->get('detail_peminjaman');
		if ($query->num_rows()>0) {
			return $query->row()->jml;
		}else{
			return 0;
		}
	}
	function jumlah42(){
		$this->db->select_sum('jml');
		$query=$this->db->get('detail_gpeminjaman');
		if ($query->num_rows()>0) {
			return $query->row()->jml;
		}else{
			return 0;
		}
	}
	function jumlah43(){
		$this->db->select_sum('jml2');
		$query=$this->db->get('detail_peminjaman');
		if ($query->num_rows()>0) {
			return $query->row()->jml2;
		}else{
			return 0;
		}
	}
	function jumlah44(){
		$this->db->select_sum('jml2');
		$query=$this->db->get('detail_gpeminjaman');
		if ($query->num_rows()>0) {
			return $query->row()->jml2;
		}else{
			return 0;
		}
	}
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
 
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function update_data2($where1,$where2,$data2,$table){
		$this->db->query("update $table set jml2=$data2 where id_pinjam=$where1 and kode_buku=$where2");
	}
	function update_data3($where1,$where2,$data2,$table){
		$this->db->query("update $table set jml2=$data2 where id_gpinjam=$where1 and kode_buku=$where2");
	}
	function update_data_b($where,$data_b,$table){
		$this->db->where($where);
		$this->db->update($table,$data_b);
	}

	//laporan//
	function lpengunjung($where,$tabel){
		return $this->db->query("select * from $tabel inner join siswa on pengunjung.nis=siswa.nis where tanggal like'$where%'");
	}

	function pengunjung_bulanan($bulan,$tahun,$tabel){
		return $this->db->query("select * from pengunjung inner join siswa on pengunjung.nis=siswa.nis where tanggal like'%$tahun-$bulan%'");
	}
	function pengunjung_bulanan2($jurusan,$bulan,$tahun,$tabel){
		return $this->db->query("select * from pengunjung inner join siswa on pengunjung.nis=siswa.nis where tanggal like'%$tahun-$bulan%' and siswa.id_kejur like'%$jurusan%'");
	}
	// function lpengunjung($where,$where2,$tabel){
	// 	return $this->db->query("select * from $tabel inner join siswa on pengunjung.nis=siswa.nis where DATE_FORMAT(tanggal,'%Y-%m-%d') >= '$where' AND DATE_FORMAT(tanggal,'%Y-%m-%d')<= '$where2'");
	// }
	
	function lgpengunjung($where,$where2){
		return $this->db->query("select * from gpengunjung inner join guru on gpengunjung.no_anggota=guru.no_anggota where DATE_FORMAT(tanggal,'%Y-%m-%d') >= '$where' AND DATE_FORMAT(tanggal,'%Y-%m-%d') <= '$where2'");
	}
	function lpeminjam($where,$tabel){
		return $this->db->query("select * from siswa inner join peminjam on siswa.nis=peminjam.nis inner join detail_peminjaman on peminjam.id_pinjam=detail_peminjaman.id_pinjam inner join buku on detail_peminjaman.kode_buku=buku.kode_buku where tgl_pinjam='$where'" );
	}
	function lpeminjam1(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_kembali='$date' and p.status=1");
	}
	function lpeminjam2(){
		date_default_timezone_set('Asia/Jakarta');
		$date= date('Y-m-d');
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_kembali<'$date' and p.status=1");
	}
	function peminjam_bulanan0($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='REF'");
	}
	function peminjam_bulanan1($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='000'");
	}
	function peminjam_bulanan2($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='100'");
	}
	function peminjam_bulanan3($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='200'");
	}
	function peminjam_bulanan4($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='300'");
	}
	function peminjam_bulanan5($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='400'");
	}
	function peminjam_bulanan6($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='500'");
	}
	function peminjam_bulanan7($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='600'");
	}
	function peminjam_bulanan8($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='700'");
	}
	function peminjam_bulanan9($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='800'");
	}
	function peminjam_bulanan10($a,$tgl,$bulan,$tahun,$tabel){
		return $this->db->query("select * from siswa s inner join peminjam p on s.nis=p.nis inner join detail_peminjaman d on p.id_pinjam=d.id_pinjam inner join buku b on d.kode_buku=b.kode_buku where p.tgl_pinjam='$tahun-$bulan-$a$tgl' and b.kode_kategori='900'");
	}

}
?>  