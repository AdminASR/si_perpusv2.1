<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_Event extends CI_Model {
  // Fungsi untuk menampilkan semua data event
  public function view(){
    return $this->db->query("select * from event");
  }

  public function view_limit(){
    return $this->db->query("select * from event orders limit 2");
  }

   public function utama(){
    return $this->db->query("select * from event where status=1");
  }

  function tambah($data){
     $this->db->insert('event',$data);
    return TRUE;
  }
  public function ubah($data,$where){
    $this->db->update('event',$data,$where);
    return TRUE;
  }
  public function hapus($where){
    $this->db->where($where);
    $this->db->delete('event');
    return TRUE;
  }
  public function update_data($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  }
  function detail_data_1($where,$table){
    return $this->db->get_where($table,$where);
  }


  function detail_data_2($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  }

   function hapus_data($where,$table){
    $this->db->where($where);
    $this->db->delete($table);
  } 
}